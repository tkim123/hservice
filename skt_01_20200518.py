#!/root/anaconda3/bin/python
'''서비스 대상자별 시간대별 재실여부 판단 입력
    1. 서비스 대상자 자택 집주소의 위경도 (/shared/data/common/common_siteid_yyyymmdd.csv)
    2. 서비스 대상자의 시간대별 통화건수, SMS, 데이터 사용량, 위도, 경도 (셀기반) 
        (/shared/data/skt/skt_siteid_yyyymmdd_in_{1,2}.csv)
    3. 서비스 대상자의 시간대별 GPS (6분간격저장 (/shared/data/skt/other_siteid_yyyymmdd_in.csv))

결과 파일 저장 : /shared/data/skt/com_siteid_yyyymmdd_out.csv (휴대폰번호,일자,시간,통화건수,sms발신건수,데이터사용량,위도,경도,재실여부)
재실여부 : 0 재실, 1 외출
'''

import sys,os
import pandas as pd
import numpy as np
import random
import collections
from tqdm import tqdm
import pickle
import itertools
import mysql.connector as mariadb
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
from hpkg.hservice_util2 import *

siteid="01"
print("="*20)
print("JOB START")

# 0. 데이터분석 일자
#ymd=date.today().strftime("%Y%m%d")
# ymd="20191116"
# ymd="20191117"
# ymd="20191118"
# ymd="20191119"
# ymd="20191120"
# ymd="20191121"
# ymd="20191122"
# ymd="20191123"
# ymd="20191124"
# ymd="20191125"
# ymd="20191126"
# ymd="20191127"
# ymd="20191128"
# ymd="20191129"
# ymd="20191130"
# ymd="20191201"

ymd=date.today().strftime("%Y%m%d")
yy=int(utilfuncs.genFNM(siteid,ymd,1)[10:14])
mm=int(utilfuncs.genFNM(siteid,ymd,1)[14:16])
ddt=int(utilfuncs.genFNM(siteid,ymd,1)[16:18])
print(ymd)
print(yy,mm,ddt)

# 0. input file check
f1=utilfuncs.checkfile(siteid,ymd,1)
f2=utilfuncs.checkfile(siteid,ymd,2)
f3=utilfuncs.checkfile(siteid,ymd,3)
f4=utilfuncs.checkfile(siteid,ymd,4)
f5=utilfuncs.checkfile(siteid,ymd,5)
f6=utilfuncs.checkfile(siteid,ymd,6)
f8=utilfuncs.checkfile(siteid,ymd,8)
checkNum=f1+f2+f3+f4+f5+f6
print(checkNum)

if checkNum==6:
    # 1. 서비스 대상자
    add_info=pd.read_csv("/shared/data/common/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")
    add_info=add_info.rename(columns={"phone":"mbl_phon_num"})

    # 2. Read DT input files
    dt_info11=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,2),sep=",",header=None)
    dt_info12=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,3),sep=",",header=None)
    dt_info21=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,4),sep=",",header=None)
    dt_info22=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,5),sep=",",header=None)
    dt_info11.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info21.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info12.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]
    dt_info22.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]

    skt_input=pd.concat([dt_info11,dt_info21],axis=0)
    skt_feature=pd.concat([dt_info12,dt_info22],axis=0)

    # reformat hh
    hhlst=list(map(str,skt_feature.hh))
    hhlst2=[]
    for hdx in hhlst:
        if len(hdx)==1:
            hhlst2.append('0'+hdx)
        else:
            hhlst2.append(hdx)
    skt_feature.hh=pd.Series(hhlst2)
    skt_feature=skt_feature[skt_feature.hh!="\\N"]

    print(skt_input.dt.unique())
    print(skt_feature.dt.unique())

    threshold=200
    # yy=int(ymd[0:4])
    # mm=int(ymd[4:6])
    # ddt=int(ymd[6:8])
    skt_total=utilfuncs.Load_Data_SKT(skt_input,skt_feature,threshold,yy,mm,ddt)
    skt_total=skt_total[skt_total.latitude_nm!="cp"]
    skt_total=skt_total.reset_index(drop=True)

    #reformat phone number
    skt_total["phone_num"]=pd.Series(list(map(str,skt_total.mbl_phon_num)))
    skt_total["mbl_phon_num"]=skt_total.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
    skt_total=skt_total.drop(columns=["phone_num"])

    #reformat hh
    skt_total["hh"]=pd.Series(list(map(int,skt_total.hh)))

    # Final DT info 생성
    dt_info=skt_total[["dt","hh","mbl_phon_num","total_traffic","call_send_cnt","sms_send_cnt","latitude_nm","longitude_nm","home_yn"]]
    dt_info=dt_info.reset_index(drop=True)
    #dt_info=dt_info[dt_info.dt==(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")]
    #dt_info["dt"]=(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")
    dt_info["dt"]=pd.to_datetime(dt_info["dt"]).dt.date
    dt_info["home_yn"]=np.where(dt_info.home_yn=="Y",0,1)

    # 3. Read GPS Info
    gps_info=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,6),sep=",")
    '''newly added on 20-Nov due to the format change of gps info columns'''
    gps_info=gps_info[["phone","date","time","lat","lng"]]
    gpsDF=gps_info.rename(columns={"date":"dt","time":"hh","lat":"hlat","lng":"hlon","phone":"mbl_phon_num"})
    gpsDF["dt"]=(datetime(yy,mm,ddt)).strftime("%Y%m%d")
    gpsDF["dt"]=pd.to_datetime(gpsDF["dt"]).dt.date
    
    # merge fDF, add_info by phone number --> generate home_yn
    mDF=pd.merge(gpsDF,add_info[["mbl_phon_num","lat","lng"]],on="mbl_phon_num",how="left")
    mDF["home_distance"]=mDF[["hlat","hlon","lat","lng"]].apply(lambda row:utilfuncs.calcDist(row["hlat"],row["hlon"],row["lat"],row["lng"]),axis=1)
    mDF["home_yn"]=np.where(mDF.home_distance<100,0,1)
    mDF=mDF.rename(columns={"phone":"mbl_phon_num","lat":"home_lat","lng":"home_lon","hlat":"latitude_nm","hlon":"longitude_nm"})
    # Reorder columns
    mDF=mDF[["hh","dt","mbl_phon_num","latitude_nm","longitude_nm","home_yn"]]

    # 4. 결과 csv 파일 생성
    s1=set(add_info.mbl_phon_num.unique())
    s2=set(dt_info.mbl_phon_num.unique())
    s3=set(mDF.mbl_phon_num.unique())

    # select K1 class (add_info, skt_info Not in GPS)
    k1=(s1.intersection(s2)).difference(s3)
    k1DF=dt_info[dt_info["mbl_phon_num"].isin(k1)]

    # select K2 class (add_info, skt_info, GPS)
    k2=(s1.intersection(s2)).intersection(s3)
    tmp1=dt_info[dt_info["mbl_phon_num"].isin(k2)]
    tmp1=tmp1.drop(columns=["latitude_nm","longitude_nm","home_yn"])
    tmp2=mDF[mDF["mbl_phon_num"].isin(k2)]

    k2DF=pd.merge(tmp2,tmp1,on=["dt","hh","mbl_phon_num"],how="left")
    
    # select K3 class (add_info, GPS Not in skt_info)
    k3=(s1.intersection(s3).difference(s2))
    k3DF=mDF[mDF["mbl_phon_num"].isin(k3)]

    # Exclude k1 class in the result --> newly added on 30-Jan-2020
    # mDF=pd.concat([k1DF,k2DF,k3DF],axis=0,sort=False)
    mDF=pd.concat([k2DF,k3DF],axis=0,sort=False)
    mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn"]]
    mDF=mDF.sort_values(by=["mbl_phon_num","hh"])
    # convert GB to KB
    mDF["total_traffic"] = mDF["total_traffic"].astype("float")*1000000

    # non-skt customers' mno into added -- on 21-Apr-2020
    if f8==1:
        notskt=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,8),sep=",",header=None)
        notskt.columns=["mbl_phon_num","call_send_cnt","sms_send_cnt","total_traffic","hh","dt"]
        notskt=notskt.reset_index(drop=True)

        #reformat columns
        #notskt=utilfuncs.reformat_hh(notskt)
        
        notskt.dt=pd.Series(list(map(str,notskt.dt)))
        notskt["dt"]=pd.to_datetime(notskt["dt"]).dt.date

        #reformat phone number
        notskt["phone_num"]=pd.Series(list(map(str,notskt.mbl_phon_num)))
        notskt["mbl_phon_num"]=notskt.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
        notskt=notskt.drop(columns=["phone_num"])

        #update mDF'mno columns with  notskt's
        a1=mDF.set_index(["mbl_phon_num","dt","hh"])
        a2=notskt.set_index(["mbl_phon_num","dt","hh"])
        a1.update(a2)
        mDF=a1.reset_index()
    mDF.to_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",",index=False )

    # 5. 결과 DB 적재 (communication, home tables)
    mDF=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",")
    mDF["siteID"]=siteid
    ddt=mDF.dt.unique()[0]
    print(ddt, len(mDF.dt.unique()))

    commutbl=mDF[["mbl_phon_num","hh","latitude_nm","longitude_nm","call_send_cnt","sms_send_cnt","total_traffic","dt","siteID"]]
    commutbl=commutbl.rename(columns={"mbl_phon_num":"phone","hh":"time","latitude_nm":"lat","longitude_nm":"lng","call_send_cnt":"call","sms_send_cnt":"sms","total_traffic":"data","dt":"measurementDate"})
    commutbl=commutbl.fillna(-999)

    hometbl=mDF[["mbl_phon_num","hh","home_yn","dt","siteID"]]
    hometbl=hometbl.rename(columns={"mbl_phon_num":"phone","hh":"time","home_yn":"home","dt":"measurementDate"})

    mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
    utilfuncs.delData(ddt,commutbl,"communication",mariadb_connection)
    utilfuncs.loadData(commutbl,"communication",mariadb_connection)
    utilfuncs.delData(ddt,hometbl,"home",mariadb_connection)
    utilfuncs.loadData(hometbl,"home",mariadb_connection)
    print("Type1 : JOB END")
    utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,f8,ymd,siteid)

    # # # 적재 확인
    # mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
    # tsql="select * from communication"
    # commu=pd.read_sql(tsql,mariadb_connection)
    # tsql1="select * from home"
    # home=pd.read_sql(tsql1,mariadb_connection)

    # mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
    # pd.read_sql("select count(*) from communication where measurementDate='2019-11-22'",mariadb_connection)
#else:
    #print("check input files")
elif (checkNum<6 and f1==1 and f2==0 and f3==1 and f6==1) or (checkNum<6 and f1==1 and f2==1 and f3==1 and f4==0 and f6==1):
    # 1. 서비스 대상자
    add_info=pd.read_csv("/shared/data/common/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")
    add_info=add_info.rename(columns={"phone":"mbl_phon_num"})

    # 2. Read DT input files
    skt_feature=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,3),sep=",",header=None)
    skt_feature.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]

    # reformat hh
    hhlst=list(map(str,skt_feature.hh))
    hhlst2=[]
    for hdx in hhlst:
        if len(hdx)==1:
            hhlst2.append('0'+hdx)
        else:
            hhlst2.append(hdx)
    skt_feature.hh=pd.Series(hhlst2)
    dt_info=skt_feature[skt_feature.hh!="\\N"]
    dt_info=dt_info.reset_index(drop=True)

    #reformat phone number
    dt_info["phone_num"]=pd.Series(list(map(str,dt_info.mbl_phon_num)))
    dt_info["mbl_phon_num"]=dt_info.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
    dt_info=dt_info.drop(columns=["phone_num"])

    #reformat hh
    dt_info["hh"]=pd.Series(list(map(int,dt_info.hh)))
    dt_info["dt"]=(datetime(yy,mm,ddt)).strftime("%Y%m%d")
    dt_info["dt"]=pd.to_datetime(dt_info["dt"]).dt.date
  
    print(dt_info.dt.unique())

    # 3. Read GPS Info
    gps_info=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,6),sep=",")
    '''newly added on 20-Nov due to the format change of gps info columns'''
    gps_info=gps_info[["phone","date","time","lat","lng"]]
    gpsDF=gps_info.rename(columns={"date":"dt","time":"hh","lat":"hlat","lng":"hlon","phone":"mbl_phon_num"})
    gpsDF["dt"]=(datetime(yy,mm,ddt)).strftime("%Y%m%d")
    gpsDF["dt"]=pd.to_datetime(gpsDF["dt"]).dt.date
    
    # merge fDF, add_info by phone number --> generate home_yn
    mDF=pd.merge(gpsDF,add_info[["mbl_phon_num","lat","lng"]],on="mbl_phon_num",how="left")
    mDF["home_distance"]=mDF[["hlat","hlon","lat","lng"]].apply(lambda row:utilfuncs.calcDist(row["hlat"],row["hlon"],row["lat"],row["lng"]),axis=1)
    mDF["home_yn"]=np.where(mDF.home_distance<100,0,1)
    mDF=mDF.rename(columns={"phone":"mbl_phon_num","lat":"home_lat","lng":"home_lon","hlat":"latitude_nm","hlon":"longitude_nm"})
    # Reorder columns
    mDF=mDF[["hh","dt","mbl_phon_num","latitude_nm","longitude_nm","home_yn"]]

    # 4. 결과 csv 파일 생성
    s1=set(add_info.mbl_phon_num.unique())
    s2=set(dt_info.mbl_phon_num.unique())
    s3=set(mDF.mbl_phon_num.unique())

    # select K1 class (add_info, skt_info Not in GPS)
    #k1=(s1.intersection(s2)).difference(s3)
    #k1DF=dt_info[dt_info["mbl_phon_num"].isin(k1)]

    # select K2 class (add_info, skt_info, GPS)
    k2=(s1.intersection(s2)).intersection(s3)
    tmp1=dt_info[dt_info["mbl_phon_num"].isin(k2)]
    #tmp1=tmp1.drop(columns=["latitude_nm","longitude_nm","home_yn"])
    tmp2=mDF[mDF["mbl_phon_num"].isin(k2)]

    k2DF=pd.merge(tmp2,tmp1,on=["dt","hh","mbl_phon_num"],how="left")
    
    # select K3 class (add_info, GPS Not in skt_info)
    k3=(s1.intersection(s3).difference(s2))
    k3DF=mDF[mDF["mbl_phon_num"].isin(k3)]

    mDF=pd.concat([k2DF,k3DF],axis=0,sort=False)
    mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn"]]
    mDF=mDF.sort_values(by=["mbl_phon_num","hh"])
    # convert GB to KB
    mDF["total_traffic"] = mDF["total_traffic"].astype("float")*1000000

    # non-skt customers' mno info added - on 21-Apr-2020
    if f8==1:
        notskt=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,8),sep=",",header=None)
        notskt.columns=["mbl_phon_num","call_send_cnt","sms_send_cnt","total_traffic","hh","dt"]
        notskt=notskt.reset_index(drop=True)

        #reformat columns
        #notskt=utilfuncs.reformat_hh(notskt)
        
        notskt.dt=pd.Series(list(map(str,notskt.dt)))
        notskt["dt"]=pd.to_datetime(notskt["dt"]).dt.date

        #reformat phone number
        notskt["phone_num"]=pd.Series(list(map(str,notskt.mbl_phon_num)))
        notskt["mbl_phon_num"]=notskt.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
        notskt=notskt.drop(columns=["phone_num"])

        #update mDF'mno columns with  notskt's
        a1=mDF.set_index(["mbl_phon_num","dt","hh"])
        a2=notskt.set_index(["mbl_phon_num","dt","hh"])
        a1.update(a2)
        mDF=a1.reset_index()
    mDF.to_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",",index=False )

    # 5. 결과 DB 적재 (communication, home tables)
    mDF=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",")
    mDF["siteID"]=siteid
    ddt=mDF.dt.unique()[0]
    print(ddt, len(mDF.dt.unique()))

    commutbl=mDF[["mbl_phon_num","hh","latitude_nm","longitude_nm","call_send_cnt","sms_send_cnt","total_traffic","dt","siteID"]]
    commutbl=commutbl.rename(columns={"mbl_phon_num":"phone","hh":"time","latitude_nm":"lat","longitude_nm":"lng","call_send_cnt":"call","sms_send_cnt":"sms","total_traffic":"data","dt":"measurementDate"})
    commutbl=commutbl.fillna(-999)

    hometbl=mDF[["mbl_phon_num","hh","home_yn","dt","siteID"]]
    hometbl=hometbl.rename(columns={"mbl_phon_num":"phone","hh":"time","home_yn":"home","dt":"measurementDate"})

    mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
    utilfuncs.delData(ddt,commutbl,"communication",mariadb_connection)
    utilfuncs.loadData(commutbl,"communication",mariadb_connection)
    utilfuncs.delData(ddt,hometbl,"home",mariadb_connection)
    utilfuncs.loadData(hometbl,"home",mariadb_connection)
    print("Type2 : JOB END")
    utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,f8,ymd,siteid)
    
elif checkNum<6 and f1==1 and f2==1 and f3==0 and f4==1 and f6==1:
    # 1. 서비스 대상자
    add_info=pd.read_csv("/shared/data/common/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")
    add_info=add_info.rename(columns={"phone":"mbl_phon_num"})

    # 2. Read DT input files
    dt_info11=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,2),sep=",",header=None)
    dt_info21=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,4),sep=",",header=None)
    dt_info11.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info21.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    skt_input=pd.concat([dt_info11,dt_info21],axis=0)
    print(skt_input.dt.unique())
   
    threshold=200
    skt_total=utilfuncs.Load_Data_SKT_mod(skt_input,threshold,yy,mm,ddt)
    skt_total=skt_total[skt_total.latitude_nm!="cp"]
    skt_total=skt_total.reset_index(drop=True)

    #reformat phone number
    skt_total["phone_num"]=pd.Series(list(map(str,skt_total.mbl_phon_num)))
    skt_total["mbl_phon_num"]=skt_total.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
    skt_total=skt_total.drop(columns=["phone_num"])

    #reformat hh
    skt_total["hh"]=pd.Series(list(map(int,skt_total.hh)))

    # Final DT info 생성
    dt_info=skt_total[["dt","hh","mbl_phon_num","latitude_nm","longitude_nm","home_yn"]]
    dt_info=dt_info[dt_info.dt==(datetime(yy,mm,ddt)).strftime("%Y%m%d")]
    #dt_info["dt"]=(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")
    dt_info["dt"]=pd.to_datetime(dt_info["dt"]).dt.date
    dt_info["home_yn"]=np.where(dt_info.home_yn=="Y",0,1)

    # 3. Read GPS Info
    gps_info=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,6),sep=",")
    '''newly added on 20-Nov due to the format change of gps info columns'''
    gps_info=gps_info[["phone","date","time","lat","lng"]]
    gpsDF=gps_info.rename(columns={"date":"dt","time":"hh","lat":"hlat","lng":"hlon","phone":"mbl_phon_num"})
    gpsDF["dt"]=(datetime(yy,mm,ddt)).strftime("%Y%m%d")
    gpsDF["dt"]=pd.to_datetime(gpsDF["dt"]).dt.date
    
    # merge fDF, add_info by phone number --> generate home_yn
    mDF=pd.merge(gpsDF,add_info[["mbl_phon_num","lat","lng"]],on="mbl_phon_num",how="left")
    mDF["home_distance"]=mDF[["hlat","hlon","lat","lng"]].apply(lambda row:utilfuncs.calcDist(row["hlat"],row["hlon"],row["lat"],row["lng"]),axis=1)
    mDF["home_yn"]=np.where(mDF.home_distance<100,0,1)
    mDF=mDF.rename(columns={"phone":"mbl_phon_num","lat":"home_lat","lng":"home_lon","hlat":"latitude_nm","hlon":"longitude_nm"})
    # Reorder columns
    mDF=mDF[["hh","dt","mbl_phon_num","latitude_nm","longitude_nm","home_yn"]]

    # 4. 결과 csv 파일 생성
    s1=set(add_info.mbl_phon_num.unique())
    s2=set(dt_info.mbl_phon_num.unique())
    s3=set(mDF.mbl_phon_num.unique())

    # select K1 class (add_info, skt_info Not in GPS)
    k1=(s1.intersection(s2)).difference(s3)
    k1DF=dt_info[dt_info["mbl_phon_num"].isin(k1)]

    # # select K2 class (add_info, skt_info, GPS)
    # k2=(s1.intersection(s2)).intersection(s3)
    # tmp1=dt_info[dt_info["mbl_phon_num"].isin(k2)]
    # tmp1=tmp1.drop(columns=["latitude_nm","longitude_nm","home_yn"])
    # tmp2=mDF[mDF["mbl_phon_num"].isin(k2)]

    # k2DF=pd.merge(tmp2,tmp1,on=["dt","hh","mbl_phon_num"],how="left")
    
    # select K4 class (add_info, GPS Not in skt_info)
    k4=s1.intersection(s3)
    k4DF=mDF[mDF["mbl_phon_num"].isin(k4)]

    # Exclude k1 class in the result --> newly added on 30-Jan-2020
    # mDF=pd.concat([k1DF,k4DF],axis=0,sort=False)
    mDF=k4DF
    mDF["call_send_cnt"]=-999
    mDF["sms_send_cnt"]=-999
    mDF["total_traffic"]=-999
    
    mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn"]]
    mDF=mDF.sort_values(by=["mbl_phon_num","hh"])
    # convert GB to KB
    mDF["total_traffic"] = mDF["total_traffic"].astype("float")*1000000
    
    # non-skt customers' mno info added - on 21-Apr-2020
    if f8==1:
        notskt=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,8),sep=",",header=None)
        notskt.columns=["mbl_phon_num","call_send_cnt","sms_send_cnt","total_traffic","hh","dt"]
        notskt=notskt.reset_index(drop=True)

        #reformat columns
        #notskt=utilfuncs.reformat_hh(notskt)
        
        notskt.dt=pd.Series(list(map(str,notskt.dt)))
        notskt["dt"]=pd.to_datetime(notskt["dt"]).dt.date

        #reformat phone number
        notskt["phone_num"]=pd.Series(list(map(str,notskt.mbl_phon_num)))
        notskt["mbl_phon_num"]=notskt.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
        notskt=notskt.drop(columns=["phone_num"])

        #update mDF'mno columns with  notskt's
        a1=mDF.set_index(["mbl_phon_num","dt","hh"])
        a2=notskt.set_index(["mbl_phon_num","dt","hh"])
        a1.update(a2)
        mDF=a1.reset_index()
    mDF.to_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",",index=False )

    # 5. 결과 DB 적재 (communication, home tables)
    mDF=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",")
    mDF["siteID"]=siteid
    ddt=mDF.dt.unique()[0]
    print(ddt, len(mDF.dt.unique()))

    commutbl=mDF[["mbl_phon_num","hh","latitude_nm","longitude_nm","call_send_cnt","sms_send_cnt","total_traffic","dt","siteID"]]
    commutbl=commutbl.rename(columns={"mbl_phon_num":"phone","hh":"time","latitude_nm":"lat","longitude_nm":"lng","call_send_cnt":"call","sms_send_cnt":"sms","total_traffic":"data","dt":"measurementDate"})
    commutbl=commutbl.fillna(-999)

    hometbl=mDF[["mbl_phon_num","hh","home_yn","dt","siteID"]]
    hometbl=hometbl.rename(columns={"mbl_phon_num":"phone","hh":"time","home_yn":"home","dt":"measurementDate"})

    mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
    utilfuncs.delData(ddt,commutbl,"communication",mariadb_connection)
    utilfuncs.loadData(commutbl,"communication",mariadb_connection)
    utilfuncs.delData(ddt,hometbl,"home",mariadb_connection)
    utilfuncs.loadData(hometbl,"home",mariadb_connection)
    print("Type3 : JOB END")
    utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,f8,ymd,siteid)

# DT의 2개 파일이 모두 전송되지 않았을 경우 (GPS정보는 전송 되어야 함)
elif f1==1 and f2==0 and f3==0 and f6==1:
    # 1. 서비스 대상자
    add_info=pd.read_csv("/shared/data/common/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")
    add_info=add_info.rename(columns={"phone":"mbl_phon_num"})

    # 2. Can't Read DT input files

    # 3. Read GPS Info
    gps_info=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,6),sep=",")
    '''newly added on 20-Nov due to the format change of gps info columns'''
    gps_info=gps_info[["phone","date","time","lat","lng"]]
    gpsDF=gps_info.rename(columns={"date":"dt","time":"hh","lat":"hlat","lng":"hlon","phone":"mbl_phon_num"})
    gpsDF["dt"]=(datetime(yy,mm,ddt)).strftime("%Y%m%d")
    gpsDF["dt"]=pd.to_datetime(gpsDF["dt"]).dt.date
    
    # merge fDF, add_info by phone number --> generate home_yn
    mDF=pd.merge(gpsDF,add_info[["mbl_phon_num","lat","lng"]],on="mbl_phon_num",how="left")
    mDF["home_distance"]=mDF[["hlat","hlon","lat","lng"]].apply(lambda row:utilfuncs.calcDist(row["hlat"],row["hlon"],row["lat"],row["lng"]),axis=1)
    mDF["home_yn"]=np.where(mDF.home_distance<100,0,1)
    mDF=mDF.rename(columns={"phone":"mbl_phon_num","lat":"home_lat","lng":"home_lon","hlat":"latitude_nm","hlon":"longitude_nm"})
    # Reorder columns
    mDF=mDF[["hh","dt","mbl_phon_num","latitude_nm","longitude_nm","home_yn"]]

    # 4. 결과 csv 파일 생성
    s1=set(add_info.mbl_phon_num.unique())
    s3=set(mDF.mbl_phon_num.unique())
    
    # select K5 class (add_info, GPS)
    k5=s1.intersection(s3)
    k5DF=mDF[mDF["mbl_phon_num"].isin(k5)]

    mDF=k5DF
    mDF["call_send_cnt"]=-999
    mDF["sms_send_cnt"]=-999
    mDF["total_traffic"]=-999
    mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn"]]
    mDF=mDF.sort_values(by=["mbl_phon_num","hh"])
    # convert GB to KB
    mDF["total_traffic"] = mDF["total_traffic"].astype("float")*1000000

    # non-skt customers' mno info added - on 21-Apr-2020
    if f8==1:
        notskt=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,8),sep=",",header=None)
        notskt.columns=["mbl_phon_num","call_send_cnt","sms_send_cnt","total_traffic","hh","dt"]
        notskt=notskt.reset_index(drop=True)

        #reformat columns
        #notskt=utilfuncs.reformat_hh(notskt)
        
        notskt.dt=pd.Series(list(map(str,notskt.dt)))
        notskt["dt"]=pd.to_datetime(notskt["dt"]).dt.date

        #reformat phone number
        notskt["phone_num"]=pd.Series(list(map(str,notskt.mbl_phon_num)))
        notskt["mbl_phon_num"]=notskt.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
        notskt=notskt.drop(columns=["phone_num"])

        #update mDF'mno columns with  notskt's
        a1=mDF.set_index(["mbl_phon_num","dt","hh"])
        a2=notskt.set_index(["mbl_phon_num","dt","hh"])
        a1.update(a2)
        mDF=a1.reset_index()
    mDF.to_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",",index=False )

    # 5. 결과 DB 적재 (communication, home tables)
    mDF=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",")
    mDF["siteID"]=siteid
    ddt=mDF.dt.unique()[0]
    print(ddt, len(mDF.dt.unique()))

    commutbl=mDF[["mbl_phon_num","hh","latitude_nm","longitude_nm","call_send_cnt","sms_send_cnt","total_traffic","dt","siteID"]]
    commutbl=commutbl.rename(columns={"mbl_phon_num":"phone","hh":"time","latitude_nm":"lat","longitude_nm":"lng","call_send_cnt":"call","sms_send_cnt":"sms","total_traffic":"data","dt":"measurementDate"})
    commutbl=commutbl.fillna(-999)

    hometbl=mDF[["mbl_phon_num","hh","home_yn","dt","siteID"]]
    hometbl=hometbl.rename(columns={"mbl_phon_num":"phone","hh":"time","home_yn":"home","dt":"measurementDate"})

    mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
    utilfuncs.delData(ddt,commutbl,"communication",mariadb_connection)
    utilfuncs.loadData(commutbl,"communication",mariadb_connection)
    utilfuncs.delData(ddt,hometbl,"home",mariadb_connection)
    utilfuncs.loadData(hometbl,"home",mariadb_connection)
    print("Type4 : JOB END")
    utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,f8,ymd,siteid)
# # GPS정보가 전송되지 않았을 경우(다른 5개 파일은 전송됨)
# elif f1==1 and f2==1 and f3==1 and f4==1 and f5==1 and f6==0:
#     # 1. 서비스 대상자
#     add_info=pd.read_csv("/shared/data/common/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")
#     add_info=add_info.rename(columns={"phone":"mbl_phon_num"})

#     # 2. Read DT input files
#     dt_info11=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,2),sep=",",header=None)
#     dt_info12=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,3),sep=",",header=None)
#     dt_info21=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,4),sep=",",header=None)
#     dt_info22=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,5),sep=",",header=None)
#     dt_info11.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
#     dt_info21.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
#     dt_info12.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]
#     dt_info22.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]

#     skt_input=pd.concat([dt_info11,dt_info21],axis=0)
#     skt_feature=pd.concat([dt_info12,dt_info22],axis=0)

#     # reformat hh
#     hhlst=list(map(str,skt_feature.hh))
#     hhlst2=[]
#     for hdx in hhlst:
#         if len(hdx)==1:
#             hhlst2.append('0'+hdx)
#         else:
#             hhlst2.append(hdx)
#     skt_feature.hh=pd.Series(hhlst2)
#     skt_feature=skt_feature[skt_feature.hh!="\\N"]

#     print(skt_input.dt.unique())
#     print(skt_feature.dt.unique())

#     threshold=200
#     # yy=int(ymd[0:4])
#     # mm=int(ymd[4:6])
#     # ddt=int(ymd[6:8])
#     skt_total=utilfuncs.Load_Data_SKT(skt_input,skt_feature,threshold,yy,mm,ddt)
#     skt_total=skt_total[skt_total.latitude_nm!="cp"]
#     skt_total=skt_total.reset_index(drop=True)

#     #reformat phone number
#     skt_total["phone_num"]=pd.Series(list(map(str,skt_total.mbl_phon_num)))
#     skt_total["mbl_phon_num"]=skt_total.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
#     skt_total=skt_total.drop(columns=["phone_num"])

#     #reformat hh
#     skt_total["hh"]=pd.Series(list(map(int,skt_total.hh)))

#     # Final DT info 생성
#     dt_info=skt_total[["dt","hh","mbl_phon_num","total_traffic","call_send_cnt","sms_send_cnt","latitude_nm","longitude_nm","home_yn"]]
#     dt_info=dt_info.reset_index(drop=True)
#     #dt_info=dt_info[dt_info.dt==(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")]
#     #dt_info["dt"]=(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")
#     dt_info["dt"]=pd.to_datetime(dt_info["dt"]).dt.date
#     dt_info["home_yn"]=np.where(dt_info.home_yn=="Y",0,1)

#     # 3. Can't Read GPS Info

#     # 4. 결과 csv 파일 생성
#     s1=set(add_info.mbl_phon_num.unique())
#     s2=set(dt_info.mbl_phon_num.unique())
#     #s3=set(mDF.mbl_phon_num.unique())

#     # select K11 class (add_info, skt_info)
#     k1=s1.intersection(s2)
#     k1DF=dt_info[dt_info["mbl_phon_num"].isin(k1)]

#     # # select K2 class (add_info, skt_info, GPS)
#     # k2=(s1.intersection(s2)).intersection(s3)
#     # tmp1=dt_info[dt_info["mbl_phon_num"].isin(k2)]
#     # tmp1=tmp1.drop(columns=["latitude_nm","longitude_nm","home_yn"])
#     # tmp2=mDF[mDF["mbl_phon_num"].isin(k2)]

#     # k2DF=pd.merge(tmp2,tmp1,on=["dt","hh","mbl_phon_num"],how="left")
    
#     # # select K3 class (add_info, GPS Not in skt_info)
#     # k3=(s1.intersection(s3).difference(s2))
#     # k3DF=mDF[mDF["mbl_phon_num"].isin(k3)]

#     mDF=k1DF
#     mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn"]]
#     mDF=mDF.sort_values(by=["mbl_phon_num","hh"])
#     mDF.to_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",",index=False )

#     # 5. 결과 DB 적재 (communication, home tables)
#     mDF=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",")
#     mDF["siteID"]=siteid
#     ddt=mDF.dt.unique()[0]
#     print(ddt, len(mDF.dt.unique()))

#     commutbl=mDF[["mbl_phon_num","hh","latitude_nm","longitude_nm","call_send_cnt","sms_send_cnt","total_traffic","dt","siteID"]]
#     commutbl=commutbl.rename(columns={"mbl_phon_num":"phone","hh":"time","latitude_nm":"lat","longitude_nm":"lng","call_send_cnt":"call","sms_send_cnt":"sms","total_traffic":"data","dt":"measurementDate"})
#     commutbl=commutbl.fillna(-999)

#     hometbl=mDF[["mbl_phon_num","hh","home_yn","dt","siteID"]]
#     hometbl=hometbl.rename(columns={"mbl_phon_num":"phone","hh":"time","home_yn":"home","dt":"measurementDate"})

#     mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
#     utilfuncs.delData(ddt,commutbl,"communication",mariadb_connection)
#     utilfuncs.loadData(commutbl,"communication",mariadb_connection)
#     utilfuncs.delData(ddt,hometbl,"home",mariadb_connection)
#     utilfuncs.loadData(hometbl,"home",mariadb_connection)
#     print("Type5 : JOB END")
#     utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,ymd,siteid)
else:
    utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,f8,ymd,siteid)
