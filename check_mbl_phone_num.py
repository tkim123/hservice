import sys,os
import pandas as pd
import numpy as np
import random
import collections
from tqdm import tqdm
import pickle
import itertools
import mysql.connector as mariadb
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
from hpkg.hservice_util2 import *

siteid="01"

# 0. 데이터분석 일자
ymd="20191116"
ymd="20191117"
ymd="20191118"
ymd="20191119"
ymd="20191120"
ymd="20191121"
ymd="20191122"
ymd=date.today().strftime("%Y%m%d")
print(ymd)

ddd=["20191116","20191117","20191118","20191119","20191120","20191121","20191122","20191123"]
checknum=[]
for ddx in ddd:
    checknum.append(checkFreq(siteid,ddx))

zzz=pd.DataFrame(checknum,columns=["ymd","common","skt1","skt2","other","total_common"])
zzz.to_csv("/root/hservice/freqcheck.csv",index=False,encoding="utf-8",sep=",")


def checkFreq(siteid,ymd):
    add_info=pd.read_csv("/shared/data/common/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")
    skt1=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,2),sep=",",header=None)
    skt2=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,3),sep=",",header=None)
    skt1.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    skt2.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]

    #reformat phone number
    skt1["phone_num"]=pd.Series(list(map(str,skt1.mbl_phon_num)))
    skt1["mbl_phon_num"]=skt1.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
    skt1=skt1.drop(columns=["phone_num"])

    skt2["phone_num"]=pd.Series(list(map(str,skt2.mbl_phon_num)))
    skt2["mbl_phon_num"]=skt2.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
    skt2=skt2.drop(columns=["phone_num"])

    gps_info=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,6),sep=",")

    s1=set(add_info.phone.unique())
    s2=set(skt1.mbl_phon_num.unique())
    s3=set(skt2.mbl_phon_num.unique())
    s4=set(gps_info.phone.unique())

    d1=len(s1)
    d2=len(s2)
    d3=len(s3)
    d4=len(s4)
    d5=len(s1.intersection(s2).intersection(s3).intersection(s4))
    return (ymd,d1,d2,d3,d4,d5)
