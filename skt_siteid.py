'''서비스 대상자별 시간대별 재실여부 판단 입력
    1. 서비스 대상자 자택 집주소의 위경도 (/shared/data/common/common_siteid_yyyymmdd.csv)
    2. 서비스 대상자의 시간대별 통화건수, SMS, 데이터 사용량, 위도, 경도 (셀기반) (/shared/data/skt/skt_siteid_yyyymmdd_in.csv)
    3. 서비스 대상자의 시간대별 GPS (6분간격저장 (/shared/data/skt/other_siteid_yyyymmdd_in.csv))

결과 파일 저장 : /shared/data/skt/com_siteid_yyyymmdd_out.csv (휴대폰번호,일자,시간,통화건수,sms발신건수,데이터사용량,위도,경도,재실여부)
재실여부 : 0 재실, 1 외출
'''

import sys,os
import pandas as pd
import numpy as np
import mysql.connector as mariadb
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
from hpkg.hservice_util import *

siteid="04"

add_info=pd.read_csv("/shared/data/common/"+utilfuncs.genFNM(siteid,1),sep=",")
#generate dt_info
dt_info=pd.read_csv("/root/hservice/DT_center/skt_sample.csv",sep=",")
dt_info=dt_info.drop(columns=["Unnamed: 0"])
dt_info.mbl_phon_num="010-5265-5483"
dt_info.hh=np.arange(20)
dt_info["total_traffic"]=10
dt_info["call_send_cnt"]=10
dt_info["sms_send_cnt"]=10
dt_info.dt="2019-11-11"
dt_info.to_csv("/shared/data/skt/skt_04_20191111_in.csv",sep=",",index=False)
#
dt_info=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,2),sep=",")
dt_info["dt"]=pd.to_datetime(dt_info["dt"]).dt.date

gps_info=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,3),sep=",")
gps_info["hh"]=pd.to_datetime(gps_info["date"]).dt.hour
gps_info["date2"]=pd.to_datetime(gps_info["date"]).dt.date

# calculate the representative GPS for each hour
hhs=gps_info.hh.unique()
flst=[]
for hdx in np.arange(len(hhs)):
    dd=gps_info[gps_info.hh==hhs[hdx]]
    dd=dd.reset_index(drop=True)
    mednum=np.where(dd[-3:].lat==np.median(dd[-3:].lat))[0][0]

    lval=(dd.phone[mednum],dd.date2[mednum],dd.hh[mednum],dd.lat[mednum],dd.lng[mednum])
    flst.append(lval)
fDF=pd.DataFrame(flst,columns=["phone","dt","hh","hlat","hlon"])

# merge fDF, add_info by phone number
mDF=pd.merge(fDF,add_info[["phone","lat","lng"]],on="phone",how="left")
mDF["home_distance"]=mDF[["hlat","hlon","lat","lng"]].apply(lambda row:utilfuncs.calcDist(row["hlat"],row["hlon"],row["lat"],row["lng"]),axis=1)
mDF["home_yn"]=np.where(mDF.home_distance<100,0,1)
mDF=mDF.rename(columns={"phone":"mbl_phon_num","lat":"home_lat","lng":"home_lon","hlat":"latitude_nm","hlon":"longitude_nm"})

mDF=mDF[["hh","dt","mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","home_distance","home_yn"]]

#merge mDF and dt_info by phone number
mDF=pd.merge(mDF,dt_info[["dt","hh","mbl_phon_num","total_traffic","call_send_cnt","sms_send_cnt"]],on=["dt","hh","mbl_phon_num"],how="left")

mDF.to_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,4),sep=",",index=False )

# mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
# tsql="select * from communication"
# commu=pd.read_sql(tsql,mariadb_connection)
# tsql1="select * from home"
# home=pd.read_sql(tsql1,mariadb_connection)


# file name for Raw Data
# skt_siteid_yyyymmdd_in.csv

# Read Raw Data by siteid
tst_fnm="/root/hservice/"+utilfuncs.genFNM("01")
tst_df=pd.read_csv(tst_fnm,sep=",")
tst_df=tst_df.reset_index(drop=True)
tst_df.shape

#
# append tst_df to communication
#
mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
utilfuncs.loadData(tst_df,"communication",mariadb_connection)

#
# append tst_df2 to home
#
tst_df2=tst_df[["phone","time","measurementDate"]]
tst_df2["home"] = -1
tst_df2=tst_df2[["phone","time","home","measurementDate"]]

mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
utilfuncs.loadData(tst_df2,"home",mariadb_connection)

