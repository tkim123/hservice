#!/root/anaconda3/bin/python
'''hourly batch
    분석 결과 파일 생성 되지 않았을 경우를 체크하여 시간별 batch
'''
import sys,os
import pandas as pd
import numpy as np
import random
import collections
from tqdm import tqdm
import pickle
import itertools
import mysql.connector as mariadb
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
from hpkg.hservice_util2 import *

#
# READ service_site_info
con = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="silver_care")
tsql = "select * from service_site_info"
stbl = pd.read_sql(tsql,con)
stbl = stbl.reset_index(drop="True")

site_lst = list(stbl[stbl.flag=="Y"].site)

for sidx in site_lst:
    #check current date and time
    now=datetime.now().strftime("%H:%M:%S")
    ymd=date.today().strftime("%Y%m%d")
    yy=int(utilfuncs.genFNM(sidx,ymd,1)[10:14])
    mm=int(utilfuncs.genFNM(sidx,ymd,1)[14:16])
    ddt=int(utilfuncs.genFNM(sidx,ymd,1)[16:18])
    
    print("="*41)
    print("siteid: "+sidx+" Hourly JOB START : "+ymd+" "+now[0:2])
    # print("="*20)
    # print(ymd)
    print(yy,mm,ddt)

    ## 00. delete log files by every the first day of a month
    if ymd[6:]=="01":
        os.system("rm -rf /root/h_batch_01.log")

    # 0. result file check
    rchk=utilfuncs.checkfile(sidx,ymd,7)
    if rchk==1:
        print("Result File was alread generated!!")
    else:
        os.system("/root/hservice/code_template.py "+ sidx)
