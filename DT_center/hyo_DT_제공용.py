# basic function
import pandas as pd
import numpy  as np
import random
from tqdm import tqdm
import pickle
import itertools
from datetime import timedelta, date

# 거리함수
def haversine(lat1, lon1, lat2, lon2):
    
    '''
    haversine distance
    '''
    MILES = 6371
    lat1, lon1, lat2, lon2 = map(np.deg2rad, [lat1, lon1, lat2, lon2])
    dlat = lat2 - lat1 
    dlon = lon2 - lon1 
    a = np.sin(dlat/2)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2)**2
    c = 2 * np.arcsin(np.sqrt(a)) 
    total_miles = MILES * c
    return total_miles

# 재실여부 판단 함수
def Load_Data_SKT(skt_input, address_input, threshold, start_yy, start_mm, start_dd, end_yy, end_mm, end_dd):
    
    '''
    Load_data_SKT
    Provided by DT Center
    =====

    Provides:
      1. 고객 n명, p시간 (0~23), q일 (1일~31일)에 대한 모든 위경도 좌표, 거주지 좌표, 재택여부 정보
      2. SKT 시간대별 위치 데이터 보정 
      3. 현재 위치와 거주지 위치 사이의 거리를 계산
    
    How to use?:
      1. skt 위치 데이터 (skt_input) 를 loading (['svc_mgmt_num','dt','hh','latitude_nm','longitude_nm','mbl_phon_num'] 형태로 제공)
      2. 고객 주소 데이터 (address_input) 를 loading (['mbl_phon_num','home_lat','home_lon'] 형태로 제공)
      3. Load_Data_SKT 실행

        :param skt_input: skt 위치 데이터
        :param address_input: 고객 주소 데이터 
        :param threshold: (재택여부 판단을 위한) 현 위치와 거주지 위치 사이 거리 임계점(km)
        :param start_yy, start_mm, stard_dd: 데이터 start 시점 (e.g., 2019, 7, 1)
        :param end_yy, end_mm, end_dd: 데이터 end 시점 (e.g., 2019, 7, 31)


    Example: ''output = Load_Data_SKT(skt_input=skt, address_input=kepco, threshold=0.5, start_yy=2019, start_mm=7, start_dd=1, end_yy=2019, end_mm=7, end_dd=31)''
        
    Return: ['hh','dt','mbl_phon_num','latitude_nm','longitude_nm','home_lat','home_lon','home_distance','home_yn'] 형태의 DataFrame 
    
    '''
    
    #1. 모든 날짜, 시간에 대한 기준테이블 생성 
    # 모든시간
    iterable1 = np.array(['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23'])
    
    # 모든날짜
    iterable2 = pd.date_range(date(start_yy,start_mm,start_dd), date(end_yy,end_mm,end_dd), freq = '1d')
    iterable2 = iterable2.union([iterable2[-1]])
    iterable2 = [d.strftime("%Y%m%d") for d in iterable2] 
    iterable2 = np.array(iterable2,dtype=object)
    
    # 모든전화번호                      
    iterable3 = skt_input['mbl_phon_num'].unique()
    
    # 시간, 날짜, 전화번호 조합으로 만든 기준테이블 
    item = [iterable1,iterable2,iterable3]
    standard = pd.DataFrame(list(itertools.product(*item)),columns=['hh','dt','mbl_phon_num'])
    standard = standard.sort_values(by=['mbl_phon_num','dt','hh']).reset_index(drop=True)
    
    
    #2. skt테이블 + 주소 테이블 
    input_1 = pd.merge(skt_input, address_input, how='left',on='mbl_phon_num')
    
    #3. 집과의 거리 계산
    input_1[['longitude_nm','latitude_nm','home_lon','home_lat']] = input_1[['longitude_nm','latitude_nm','home_lon','home_lat']].astype(float)

    input_1['home_distance'] = input_1[['latitude_nm','longitude_nm','home_lat','home_lon']] \
    .apply(lambda row:haversine(row['longitude_nm'],row['latitude_nm'],row['home_lon'],row['home_lat']), axis=1)
    #.apply(lambda row:haversine((row['longitude_nm'],row['latitude_nm']),(row['home_lon'],row['home_lat'])), axis=1)
    input_1['home_yn'] = input_1['home_distance'].apply(lambda x: 'Y' if x < threshold else 'N')

    #4. 기준테이블 + skt테이블 + 주소테이블 
    input_2 = pd.merge(standard,input_1,left_on=['mbl_phon_num','dt','hh'] \
                         ,right_on=['mbl_phon_num','dt','hh'],how='left') \
                         .sort_values(by=['mbl_phon_num','dt','hh']).reset_index(drop=True)
                         
    input_2['tmp'] = input_2['mbl_phon_num'].astype(int)
    
                         
    #5. 비는 시간의 데이터는 이전 시간데이터로 대체                      
    input_2 = input_2.fillna('cp')
    input_3 = input_2.copy(deep=True)
    input_3[['longitude_nm','latitude_nm','home_lon','home_lat','home_distance']] = input_3[['longitude_nm','latitude_nm','home_lon','home_lat','home_distance']].astype(str)
    # 값 대체
    output = pd.DataFrame(columns=input_2.columns)
    # 사람별로 나눠서
    for i in input_3['mbl_phon_num'].unique():
        tmp = input_3[input_3['mbl_phon_num']==i].reset_index(drop=True)
        # 계속 업데이트 되면서 값 대체 
        for j in tqdm(range(1,tmp.shape[0]),mininterval=0.1,position=0, leave=True):
            tmp = tmp.set_value(j,'latitude_nm',tmp['latitude_nm'].loc[j].replace('cp',tmp['latitude_nm'].loc[j-1]))
            tmp = tmp.set_value(j,'longitude_nm',tmp['longitude_nm'].loc[j].replace('cp',tmp['longitude_nm'].loc[j-1]))
            tmp = tmp.set_value(j,'home_yn',tmp['home_yn'].loc[j].replace('cp',tmp['home_yn'].loc[j-1]))
            
            tmp = tmp.set_value(j,'home_lat',tmp['home_lat'].loc[j].replace('cp',tmp['home_lat'].loc[j-1]))
            tmp = tmp.set_value(j,'home_lon',tmp['home_lon'].loc[j].replace('cp',tmp['home_lon'].loc[j-1]))
            tmp = tmp.set_value(j,'home_distance',tmp['home_distance'].loc[j].replace('cp',tmp['home_distance'].loc[j-1]))
        output = output.append(tmp)

    output = output.reset_index(drop=True)
    output = output[['hh','dt','mbl_phon_num','latitude_nm','longitude_nm','home_lat','home_lon','home_distance','home_yn']]
                         
    return output