import sys,os
import pandas as pd
import numpy as np
import random
from tqdm import tqdm
import pickle
import itertools
# import mysql.connector as mariadb
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
# mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")

# new DB connection on 10-Sep-2020
# mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="silver_care")
# mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")

class utilfuncs:
    @staticmethod
    def genFNM(siteid,ymd,ind):
        #ymd=date.today().strftime("%Y%m%d")
        #ymd="20191116"
        ymd1=(datetime.strptime(ymd,"%Y%m%d")-timedelta(days=1)).strftime("%Y%m%d")
        ymd2=(datetime.strptime(ymd,"%Y%m%d")-timedelta(days=2)).strftime("%Y%m%d")
        
        if ind==1:
            filenm="common_"+ siteid +"_"+ymd1+".csv"
        elif ind==2:
            filenm="skt_"+ siteid + "_"+ymd1+"_in_1.csv"
        elif ind==3:
            filenm="skt_"+ siteid + "_"+ymd1+"_in_2.csv"
        elif ind==4:
            filenm="skt_"+ siteid + "_"+ymd2+"_in_1.csv"
        elif ind==5:
            filenm="skt_"+ siteid + "_"+ymd2+"_in_2.csv"
        elif ind==6:
            filenm="notskt_"+ siteid + "_"+ymd1+"_in_1.csv"
        elif ind==7:
            filenm="notskt_" + siteid + "_"+ymd1+"_in_2.csv"
        elif ind==8:
            filenm="com_" + siteid + "_" + ymd1+"_out_1.csv"
        elif ind==9:
            filenm="com_" + siteid + "_" + ymd1+"_out_2.csv"
        return filenm

    '''def loadData(dfnm,tblnm,con=mariadb_connection):
        cursor=con.cursor()
        for i,row in dfnm.iterrows():
            sql = "INSERT INTO " + tblnm + " VALUES "
            cursor.execute(sql + str(tuple(row)))
            con.commit()'''

    def get_cols(dfnm):
        s_cols="("
        for i in range(len(dfnm.columns)):
            if i < len(dfnm.columns)-1:
                s_cols = s_cols+dfnm.columns[i]+","
            else:
                s_cols = s_cols+dfnm.columns[i]+")"
        return s_cols

    #def loadData_v2(dfnm,s_cols, tblnm,con=mariadb_connection):
     #   cursor=con.cursor()
      #  for i,row in dfnm.iterrows():
       #     sql = "INSERT INTO " + tblnm +" "+s_cols + " VALUES "
        #    cursor.execute(sql + str(tuple(row)))
         #   con.commit()

    # def delData(ddt,tblnm,con=mariadb_connection):
    #     cursor=con.cursor()
    #     delsql="delete from " + tblnm + " where measurementDate='"+ddt+"'"
    #     cursor.execute(delsql,mariadb_connection)
    #     cursor.execute("commit")

    '''def delData(ddt,dftbl,tblnm,con=mariadb_connection):
        phonenm=str(list(dftbl.mbl_phon_num))
        phonenm=phonenm.replace("[","")
        phonenm=phonenm.replace("]","")

        cursor=con.cursor()
        delsql="delete from "+tblnm+" where dt='"+ddt+"' and mbl_phon_num in ("+phonenm+")"
        cursor.execute(delsql,con)
        cursor.execute("commit")

    def delData_v2(ddt,dftbl,tblnm,con=mariadb_connection):
        phonenm=str(list(dftbl.mbl_phon_num))
        phonenm=phonenm.replace("[","")
        phonenm=phonenm.replace("]","")

        cursor=con.cursor()
        delsql="delete from "+tblnm+" where date='"+ddt+"' and mbl_phon_num in ("+phonenm+")"
        cursor.execute(delsql,con)
        cursor.execute("commit")'''

    def calcDist(x1,x2,y1,y2):
        return geodesic((x1,x2),(y1,y2)).meters

    def makeNum(pnum):
        p1=pnum[:2]
        numlen=len(pnum)-6
        p2=pnum[2:(numlen+2)]
        p3=pnum[(numlen+2):]
        return '0'+p1+'-'+p2+'-'+p3

    #거리함수
    def haversine(lat1, lon1, lat2, lon2):

        '''
        haversine distance
        '''
        radius = 6371
        lat1, lon1, lat2, lon2 = map(np.deg2rad, [lat1, lon1, lat2, lon2])
        dlat = lat2 - lat1 
        dlon = lon2 - lon1 
        a = np.sin(dlat/2)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2)**2
        c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1-a))
        #c = 2 * np.arcsin(np.sqrt(a)) 
        total_miles = radius * c
        return total_miles

    # 데이터 가공 함수
    def Load_Data_SKT(skt_input, skt_feature, threshold, yy, mm, dd):    
        '''
        Load_data_SKT
        Provided by DT Center
        =====

        Provides:
          1. 해당 함수의 output은 고객 n명, p시간 (0~23), 하루치에 대한 모든 위경도 좌표, 거주지 좌표, 재택여부 정보
          2. SKT 시간대별 기지국으로 측정된 위치 데이터를 보정 
          3. 현재 위치와 거주지 위치 사이의 거리를 계산하여 특정 thredshold를 통해 재택여부 판단

        How to use?:
          1. skt 위치 데이터 (skt_input) 를 loading (['mbl_phon_num','latitude_nm','longitude_nm','home_lat','home_lon','hh','dt'] 형태로 제공)
             단, 해당 데이터는 함수의 입력 parameter인 yy-mm-dd 기준 하루 전 데이터 포함 이틀치 데이터(e.g., yy-mm-dd가 2019-7-31 일 경우 
             skt_input은 2019-7-30 ~ 2019-7-31의 데이터로 구성되어 있어야 함)
          2. skt 수발신 데이터 (skt_feature) 를 loading (['mbl_phon_num','sms_send_cnt','call_send_cnt','total_traffic','hh','dt'] 형태로 제공)
          3. Load_Data_SKT 실행

            :param skt_input: skt 위치 데이터
            :param skt_feature: skt 수발신 데이터
            :param threshold: (재택여부 판단을 위한) 현 위치와 거주지 위치 사이 거리 임계점(km)
            :param yy, mm, dd: 데이터 시점 (e.g., 2019, 7, 31)


        Example: ''output = Load_Data_SKT(skt_input=skt, skt_feature=skt_feature, threshold=0.5, yy=2019, mm=7, dd=31)''

        Return: ['hh','dt','mbl_phon_num','latitude_nm','longitude_nm','home_lat','home_lon'
        ,'home_distance','home_yn','sms_send_cnt','call_send_cnt','total_traffic'] 형태의 DataFrame 

        '''

        #1. 모든 날짜, 시간에 대한 기준테이블 생성 
        # 모든시간
        iterable1 = np.array(['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23'])

        # 모든날짜 (하루치)
        #iterable2 = pd.date_range(date(start_yy,start_mm,start_dd), date(end_yy,end_mm,end_dd), freq = '1d')
        iterable2 = pd.date_range(date(yy,mm,dd) - timedelta(1), date(yy,mm,dd), freq = '1d')
        iterable2 = iterable2.union([iterable2[-1]])
        iterable2 = [d.strftime("%Y%m%d") for d in iterable2] 
        iterable2 = np.array(iterable2,dtype=object)

        # 모든전화번호                      
        iterable3 = skt_input['mbl_phon_num'].unique()

        # 시간, 날짜, 전화번호 조합으로 만든 기준테이블 
        item = [iterable1,iterable2,iterable3]
        standard = pd.DataFrame(list(itertools.product(*item)),columns=['hh','dt','mbl_phon_num'])
        standard = standard.sort_values(by=['mbl_phon_num','dt','hh']).reset_index(drop=True)


        #2. skt테이블 + 주소 테이블 
        #input_1 = pd.merge(skt_input, address_input, how='left',on='mbl_phon_num')
        input_1 = skt_input.copy(deep=True)
        #input_1['hh'] = input_1['hh'].astype(str)
        input_1['hh'] = input_1['hh'].apply(lambda x: '0'+str(x) if len(str(x)) ==1 else str(x))
        #3. 집과의 거리 계산
        input_1[['longitude_nm','latitude_nm','home_lon','home_lat']] = input_1[['longitude_nm','latitude_nm','home_lon','home_lat']].astype(float)

    #     input_1['home_distance'] = input_1[['latitude_nm','longitude_nm','home_lat','home_lon']] \
    #     .apply(lambda row:haversine(row['longitude_nm'],row['latitude_nm'],row['home_lon'],row['home_lat']), axis=1)

        input_1['home_distance'] = input_1[['latitude_nm','longitude_nm','home_lat','home_lon']] \
        .apply(lambda row:utilfuncs.haversine(row['latitude_nm'],row['longitude_nm'],row['home_lat'],row['home_lon']), axis=1)


        #.apply(lambda row:haversine((row['longitude_nm'],row['latitude_nm']),(row['home_lon'],row['home_lat'])), axis=1)
        input_1['home_yn'] = input_1['home_distance'].apply(lambda x: 'Y' if x < threshold else 'N')

        #4. 기준테이블 + skt테이블 + 주소테이블
        # input_1= input_1.reset_index(drop=True)
        input_1["dt"]=list(map(str,input_1.dt))
        input_2 = pd.merge(standard,input_1,left_on=['mbl_phon_num','dt','hh'] \
                             ,right_on=['mbl_phon_num','dt','hh'],how='left') \
                             .sort_values(by=['mbl_phon_num','dt','hh']).reset_index(drop=True)

        input_2['tmp'] = input_2['mbl_phon_num'].astype(int)


        #5. 비는 시간의 데이터는 이전 시간데이터로 대체                      
        input_2 = input_2.fillna('cp')
        input_3 = input_2.copy(deep=True)
        input_3[['longitude_nm','latitude_nm','home_lon','home_lat','home_distance']] = input_3[['longitude_nm','latitude_nm','home_lon','home_lat','home_distance']].astype(str)
        # 값 대체
        output = pd.DataFrame(columns=input_2.columns)
        # 사람별로 나눠서
        for i in input_3['mbl_phon_num'].unique():
            tmp = input_3[input_3['mbl_phon_num']==i].reset_index(drop=True)

            # 각 시간대별로 빈 시간대의 데이터를 채워줌 
            for j in tqdm(range(0,tmp.shape[0]),mininterval=0.1,position=0, leave=True, disable=True):

                if j != 0:
                    tmp.at[j,'latitude_nm']=tmp['latitude_nm'].loc[j].replace('cp',tmp['latitude_nm'].loc[j-1])
                    tmp.at[j,'longitude_nm']=tmp['longitude_nm'].loc[j].replace('cp',tmp['longitude_nm'].loc[j-1])
                    tmp.at[j,'home_yn']=tmp['home_yn'].loc[j].replace('cp',tmp['home_yn'].loc[j-1])
                    tmp.at[j,'home_lat']=tmp['home_lat'].loc[j].replace('cp',tmp['home_lat'].loc[j-1])
                    tmp.at[j,'home_lon']=tmp['home_lon'].loc[j].replace('cp',tmp['home_lon'].loc[j-1])
                    tmp.at[j,'home_distance']=tmp['home_distance'].loc[j].replace('cp',tmp['home_distance'].loc[j-1])
                else:
                    tmp.at[j,'latitude_nm']=tmp['latitude_nm'].loc[j].replace('cp',tmp['latitude_nm'].loc[j+1])
                    tmp.at[j,'longitude_nm']=tmp['longitude_nm'].loc[j].replace('cp',tmp['longitude_nm'].loc[j+1])
                    tmp.at[j,'home_yn']=tmp['home_yn'].loc[j].replace('cp',tmp['home_yn'].loc[j+1])
                    tmp.at[j,'home_lat']=tmp['home_lat'].loc[j].replace('cp',tmp['home_lat'].loc[j+1])
                    tmp.at[j,'home_lon']=tmp['home_lon'].loc[j].replace('cp',tmp['home_lon'].loc[j+1])
                    tmp.at[j,'home_distance']=tmp['home_distance'].loc[j].replace('cp',tmp['home_distance'].loc[j+1])

                tmp_ = tmp.loc[24:].reset_index(drop=True) # 하루전 데이터는 제외

            output = output.append(tmp_)

        output = output.reset_index(drop=True)
        output = output[['hh','dt','mbl_phon_num','latitude_nm','longitude_nm','home_lat','home_lon','home_distance','home_yn']]

        # 추가
        #skt_feature=skt_feature.reset_index(drop=True)
        skt_feature['hh'] = skt_feature['hh'].apply(lambda x: '0'+str(x) if len(str(x)) ==1 else str(x))
        skt_feature["dt"]=list(map(str,skt_feature.dt))
        skt_total = pd.merge(output,skt_feature,how='left',on=['mbl_phon_num','dt','hh']).sort_values(by=['mbl_phon_num','dt','hh']).reset_index(drop=True)
        skt_total = skt_total.fillna(0) # 데이터 사용량 등이 null일 경우는 사용량이 없는 경우임
        skt_total = skt_total[['hh','dt','mbl_phon_num','latitude_nm','longitude_nm','home_lat','home_lon','home_distance','home_yn','sms_send_cnt','call_send_cnt','total_traffic']]

        return skt_total
        
    # 데이터 가공 함수 - use only skt_input
    def Load_Data_SKT_mod(skt_input, threshold, yy, mm, dd):    
        #1. 모든 날짜, 시간에 대한 기준테이블 생성 
        # 모든시간
        iterable1 = np.array(['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23'])

        # 모든날짜 (하루치)
        #iterable2 = pd.date_range(date(start_yy,start_mm,start_dd), date(end_yy,end_mm,end_dd), freq = '1d')
        iterable2 = pd.date_range(date(yy,mm,dd) - timedelta(1), date(yy,mm,dd), freq = '1d')
        iterable2 = iterable2.union([iterable2[-1]])
        iterable2 = [d.strftime("%Y%m%d") for d in iterable2] 
        iterable2 = np.array(iterable2,dtype=object)

        # 모든전화번호                      
        iterable3 = skt_input['mbl_phon_num'].unique()

        # 시간, 날짜, 전화번호 조합으로 만든 기준테이블 
        item = [iterable1,iterable2,iterable3]
        standard = pd.DataFrame(list(itertools.product(*item)),columns=['hh','dt','mbl_phon_num'])
        standard = standard.sort_values(by=['mbl_phon_num','dt','hh']).reset_index(drop=True)


        #2. skt테이블 + 주소 테이블 
        #input_1 = pd.merge(skt_input, address_input, how='left',on='mbl_phon_num')
        input_1 = skt_input.copy(deep=True)
        #input_1['hh'] = input_1['hh'].astype(str)
        input_1['hh'] = input_1['hh'].apply(lambda x: '0'+str(x) if len(str(x)) ==1 else str(x))
        #3. 집과의 거리 계산
        input_1[['longitude_nm','latitude_nm','home_lon','home_lat']] = input_1[['longitude_nm','latitude_nm','home_lon','home_lat']].astype(float)

    #     input_1['home_distance'] = input_1[['latitude_nm','longitude_nm','home_lat','home_lon']] \
    #     .apply(lambda row:haversine(row['longitude_nm'],row['latitude_nm'],row['home_lon'],row['home_lat']), axis=1)

        input_1['home_distance'] = input_1[['latitude_nm','longitude_nm','home_lat','home_lon']] \
        .apply(lambda row:utilfuncs.haversine(row['latitude_nm'],row['longitude_nm'],row['home_lat'],row['home_lon']), axis=1)


        #.apply(lambda row:haversine((row['longitude_nm'],row['latitude_nm']),(row['home_lon'],row['home_lat'])), axis=1)
        input_1['home_yn'] = input_1['home_distance'].apply(lambda x: 'Y' if x < threshold else 'N')

        #4. 기준테이블 + skt테이블 + 주소테이블
        input_1["dt"]=list(map(str,input_1.dt))
        input_2 = pd.merge(standard,input_1,left_on=['mbl_phon_num','dt','hh'] \
                             ,right_on=['mbl_phon_num','dt','hh'],how='left') \
                             .sort_values(by=['mbl_phon_num','dt','hh']).reset_index(drop=True)

        input_2['tmp'] = input_2['mbl_phon_num'].astype(int)


        #5. 비는 시간의 데이터는 이전 시간데이터로 대체                      
        input_2 = input_2.fillna('cp')
        input_3 = input_2.copy(deep=True)
        input_3[['longitude_nm','latitude_nm','home_lon','home_lat','home_distance']] = input_3[['longitude_nm','latitude_nm','home_lon','home_lat','home_distance']].astype(str)
        # 값 대체
        output = pd.DataFrame(columns=input_2.columns)
        # 사람별로 나눠서
        for i in input_3['mbl_phon_num'].unique():
            tmp = input_3[input_3['mbl_phon_num']==i].reset_index(drop=True)

            # 각 시간대별로 빈 시간대의 데이터를 채워줌 
            for j in tqdm(range(0,tmp.shape[0]),mininterval=0.1,position=0, leave=True):
                if j != 0:
                    tmp.at[j,'latitude_nm']=tmp['latitude_nm'].loc[j].replace('cp',tmp['latitude_nm'].loc[j-1])
                    tmp.at[j,'longitude_nm']=tmp['longitude_nm'].loc[j].replace('cp',tmp['longitude_nm'].loc[j-1])
                    tmp.at[j,'home_yn']=tmp['home_yn'].loc[j].replace('cp',tmp['home_yn'].loc[j-1])
                    tmp.at[j,'home_lat']=tmp['home_lat'].loc[j].replace('cp',tmp['home_lat'].loc[j-1])
                    tmp.at[j,'home_lon']=tmp['home_lon'].loc[j].replace('cp',tmp['home_lon'].loc[j-1])
                    tmp.at[j,'home_distance']=tmp['home_distance'].loc[j].replace('cp',tmp['home_distance'].loc[j-1])
                else:
                    tmp.at[j,'latitude_nm']=tmp['latitude_nm'].loc[j].replace('cp',tmp['latitude_nm'].loc[j+1])
                    tmp.at[j,'longitude_nm']=tmp['longitude_nm'].loc[j].replace('cp',tmp['longitude_nm'].loc[j+1])
                    tmp.at[j,'home_yn']=tmp['home_yn'].loc[j].replace('cp',tmp['home_yn'].loc[j+1])
                    tmp.at[j,'home_lat']=tmp['home_lat'].loc[j].replace('cp',tmp['home_lat'].loc[j+1])
                    tmp.at[j,'home_lon']=tmp['home_lon'].loc[j].replace('cp',tmp['home_lon'].loc[j+1])
                    tmp.at[j,'home_distance']=tmp['home_distance'].loc[j].replace('cp',tmp['home_distance'].loc[j+1])

                tmp_ = tmp.loc[24:].reset_index(drop=True) # 하루전 데이터는 제외

            output = output.append(tmp_)

        output = output.reset_index(drop=True)
        output=output.fillna(0)
        skt_total = output[['hh','dt','mbl_phon_num','latitude_nm','longitude_nm','home_lat','home_lon','home_distance','home_yn']]
        return skt_total

    def checkfile(siteid,ymd,filenum):
        if filenum==1:
            filecheck=os.listdir("/shared/data/v2/user/")
        else:
            filecheck=os.listdir("/shared/data/v2/com/")
        fnum=np.where(utilfuncs.genFNM(siteid,ymd,filenum) in filecheck,1,0)

        # check whether the number of file rows is zero or not.
        if fnum==1 and filenum==1:
            if os.stat("/shared/data/v2/user/"+utilfuncs.genFNM(siteid,ymd,filenum)).st_size==0:
                fnum2=0
            else:
                fnum2=pd.read_csv("/shared/data/v2/user/"+utilfuncs.genFNM(siteid,ymd,filenum),sep=",").shape[0]
        elif fnum==1 and filenum==6:
            if os.stat("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,filenum)).st_size==0:
                fnum2=0
            else:
                fnum2=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,filenum),sep=",").shape[0]
        elif fnum==1 and any([xx==filenum for xx in [2,3,4,5,7]]):
            if os.stat("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,filenum)).st_size==0:
                fnum2=0
            else:
                fnum2=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,filenum),sep=",",header=None).shape[0]
        else:
            fnum2=fnum
        return int(np.where(fnum2==0,0,1))

    def printMsg(f1,f2,f3,f4,f5,f6,f7,ymd,siteid):
        #print("check input files")
        if f1==0:
            print("check "+utilfuncs.genFNM(siteid,ymd,1)+" !!!")
        if f2==0:
            print("check "+utilfuncs.genFNM(siteid,ymd,2)+" !!!")
        if f3==0:
            print("check "+utilfuncs.genFNM(siteid,ymd,3)+" !!!")
        if f4==0:
            print("check "+utilfuncs.genFNM(siteid,ymd,4)+" !!!")
        if f5==0:
            print("check "+utilfuncs.genFNM(siteid,ymd,5)+" !!!")
        if f6==0:
            print("check "+utilfuncs.genFNM(siteid,ymd,6)+" !!!")
        if f7==0:
            print("check "+utilfuncs.genFNM(siteid,ymd,7)+" !!!")
        if f1==1 and f2==1 and f3==1 and f4==1 and f5==1 and f6==1 and f7==1:
            print("There are all of the input files !!!")

    def reformat_hh(df):
        hhlst=list(map(str,df.hh))
        hhlst2=[]
        for hdx in hhlst:
            if len(hdx)==1:
                hhlst2.append('0'+hdx)
            else:
                hhlst2.append(hdx)
        df.hh=pd.Series(hhlst2)
        df=df[df.hh!="\\N"]
        return df

    def adjust_dist_old(dist_df, dist_limit):
        '''remove observations, the distance difference between two consecutive locations is greater than 100 km.'''
        dist_df = dist_df.sort_values("hh")
        dist_df = dist_df.reset_index(drop=True)

        #dist_limit = 100  #100killometers
        exclude_hh = list()
        for hidx in list(dist_df.index)[1:]:
            #print(hidx)
            prev_lat = float(dist_df.loc[hidx-1].latitude_nm)
            prev_lon = float(dist_df.loc[hidx-1].longitude_nm)
            curr_lat = float(dist_df.loc[hidx].latitude_nm)
            curr_lon = float(dist_df.loc[hidx].longitude_nm)

            dist_diff = abs(geodesic((prev_lat,prev_lon),(curr_lat,curr_lon)).kilometers)
            if dist_diff >= dist_limit:
                exclude_hh.append(hidx)

        if len(exclude_hh) > 0:
            dist_df = dist_df.drop(exclude_hh)
            dist_df = dist_df.reset_index(drop=True)

        return dist_df

    def adjust_dist(dist_df, dist_limit):
        '''remove observations, the distance difference between two consecutive locations is greater than 100 km.'''
        dist_df = dist_df.sort_values("hh")
        dist_df = dist_df.reset_index(drop=True)

        normal_idx=[0]
        abnormal_idx=[]
        for hidx in list(dist_df.index)[1:]:
            # print(normal_idx[-1])
            prev_lat = float(dist_df.loc[normal_idx[-1]].latitude_nm)
            prev_lon = float(dist_df.loc[normal_idx[-1]].longitude_nm)
            curr_lat = float(dist_df.loc[hidx].latitude_nm)
            curr_lon = float(dist_df.loc[hidx].longitude_nm)

            dist_diff = abs(geodesic((prev_lat,prev_lon),(curr_lat,curr_lon)).kilometers)
            if dist_diff >= dist_limit*(hidx-normal_idx[-1]):
                abnormal_idx.append(hidx)
            else:
                normal_idx.append(hidx)

        if len(abnormal_idx) > 0:
            dist_df = dist_df.drop(abnormal_idx)
            dist_df = dist_df.reset_index(drop=True)
        return dist_df
