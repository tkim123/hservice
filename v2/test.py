print("test")


if f1==1 and f2==1 and f3==1 and f4==1 and f5==1 and f6==1:
    '''skt info and gps infor are available'''

elif f1==1 and f2==1 and f3==1 and f4==1 and f5==1 and f6==0:
    '''only skt information is available'''
        # 1. 서비스 대상자
    add_info=pd.read_csv("/shared/data/v2/user/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")
    add_info=add_info.rename(columns={"phone":"mbl_phon_num"})

    # 2. Read DT input files
    dt_info11=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,2),sep=",",header=None)
    dt_info12=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,3),sep=",",header=None)
    dt_info21=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,4),sep=",",header=None)
    dt_info22=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,5),sep=",",header=None)
    dt_info11.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info21.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info12.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]
    dt_info22.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]

    skt_input=pd.concat([dt_info11,dt_info21],axis=0)
    skt_feature=pd.concat([dt_info12,dt_info22],axis=0)

    # reformat hh
    hhlst=list(map(str,skt_feature.hh))
    hhlst2=[]
    for hdx in hhlst:
        if len(hdx)==1:
            hhlst2.append('0'+hdx)
        else:
            hhlst2.append(hdx)
    # modifed on 6-Aug
    # skt_feature.hh = pd.Series(hhlst2)
    skt_feature.hh = hhlst2
    skt_feature=skt_feature[skt_feature.hh!="\\N"]

    print(skt_input.dt.unique())
    print(skt_feature.dt.unique())

    # threshold=200
    # added 21-Aug, the result unit of utilfuncs.haversine is kilometer.
    # threshold = 0.2
    # Change threshold to 1km on 24-Aug-2020
    threshold = 1.0
    # yy=int(ymd[0:4])
    # mm=int(ymd[4:6])
    # ddt=int(ymd[6:8])
    skt_total=utilfuncs.Load_Data_SKT(skt_input,skt_feature,threshold,yy,mm,ddt)
    skt_total=skt_total[skt_total.latitude_nm!="cp"]
    skt_total=skt_total.reset_index(drop=True)

    #newly added on 7-Sep
    phon_nms = skt_total.mbl_phon_num.unique()
    phon_nms_len = len(phon_nms)

    adjusted_df = pd.DataFrame()
    for pidx in phon_nms:
        dist_df = skt_total[skt_total.mbl_phon_num==pidx]
        adjusted_dist_df = utilfuncs.adjust_dist(dist_df, 100)
        adjusted_df = pd.concat([adjusted_df, adjusted_dist_df], axis=0)

    skt_total = adjusted_df.reset_index(drop=True)
    # END 7-Sep

    #reformat phone number
    skt_total["phone_num"]=pd.Series(list(map(str,skt_total.mbl_phon_num)))
    skt_total["mbl_phon_num"]=skt_total.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
    skt_total=skt_total.drop(columns=["phone_num"])

    #reformat hh
    skt_total["hh"]=pd.Series(list(map(int,skt_total.hh)))

    # Final DT info 생성
    dt_info=skt_total[["dt","hh","mbl_phon_num","total_traffic","call_send_cnt","sms_send_cnt","latitude_nm","longitude_nm","home_yn"]]
    dt_info=dt_info.reset_index(drop=True)
    #dt_info=dt_info[dt_info.dt==(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")]
    #dt_info["dt"]=(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")
    dt_info["dt"]=pd.to_datetime(dt_info["dt"]).dt.date
    dt_info["home_yn"]=np.where(dt_info.home_yn=="Y",0,1)

    # 3. Read GPS Info
    # --> No GPS Info is available.

    # 4. 결과 csv 파일 생성
    s1=set(add_info.mbl_phon_num.unique())
    s2=set(dt_info.mbl_phon_num.unique())

    # select K1 class (add_info, skt_info)
    k1=s1.intersection(s2)
    k1DF=dt_info[dt_info["mbl_phon_num"].isin(k1)]
    mDF = k1DF

    mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn"]]
    mDF=mDF.sort_values(by=["mbl_phon_num","hh"])
    # convert GB to KB
    mDF["total_traffic"] = mDF["total_traffic"].astype("float")*1000000

    # create the "power off" column
    mDF["power_off"] = np.where((mDF.total_traffic==0),0,1)

    # create statcomm_tbl
    # result0 - 정상, 1 - 관심, 2 - 주의, 3 - 경계, 4 - 심각
    statcomm_tbl = mDF[["mbl_phon_num","dt"]].drop_duplicates()
    statcomm_tbl = statcomm_tbl.reset_index(drop=True)
    statcomm_tbl["result"] = 0

    ### result1 - 관심
    mDF_tmp = mDF.copy()
    mDF_tmp["total_traffic"] = np.where((mDF.total_traffic>143),mDF.total_traffic,0)
    mDF_tmp1 = mDF_tmp.groupby("mbl_phon_num")["call_send_cnt","sms_send_cnt","total_traffic","home_yn"].sum()
    mDF_tmp1["tot_sum"] = mDF_tmp1.sum(axis=1)
    result1_lst = mDF_tmp1[mDF_tmp1.tot_sum==0].index
    statcomm_tbl["result"] = np.where((statcomm_tbl.mbl_phon_num.isin(result1_lst)),1,statcomm_tbl.result)

    ### result2 - 주의 (2일 연속 관심)
    udt = mDF.dt[0]
    udt2 = udt - timedelta(days=1)
    udt3 = udt - timedelta(days=2)

    udt_str = datetime.strftime(udt,"%Y-%m-%d")
    udt2_str = datetime.strftime(udt2,"%Y-%m-%d")
    udt3_str = datetime.strftime(udt3,"%Y-%m-%d")

    # select yesterday - data from stacomm
    tsql = "select * from statcomm where DATE_FORMAT(dt,'%Y%m%d') = STR_TO_DATE('"+udt2_str +"','%Y-%m-%d')"
    mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
    statcomm_yester = pd.read_sql(tsql,mariadb_connection_v2)
    statcomm_yester_lst = set(statcomm_yester[statcomm_yester.result==1].mbl_phon_num)
    statcomm_today_lst = set(statcomm_tbl[statcomm_tbl.result==1].mbl_phon_num)

    inter_lst = list(statcomm_yester_lst.intersection(statcomm_today_lst))
    if len(inter_lst)>0:
        df2 = statcomm_tbl[statcomm_tbl.mbl_phon_num.isin(inter_lst)].copy()
        df2["result"]=2
        statcomm_tbl.update(df2)

    ### result3 - 경계 (3일동안 장기 외출의 경우)
    mDF_tmp2 = mDF.groupby('mbl_phon_num')["home_yn"].sum()
    today_lst = set(mDF_tmp2[mDF_tmp2==24].index)

    if len(today_lst)>0:
    # select the values of home_yn during past 24 hours
        tsql2 = "select * from datacomm where DATE_FORMAT(dt,'%Y%m%d') >= STR_TO_DATE('"+udt3_str+"','%Y-%m-%d')"
        mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
        datacomm_past2 = pd.read_sql(tsql2,mariadb_connection_v2)

        if len(datacomm_past2)>0:
            dd_grp = datacomm_past2.groupby('mbl_phon_num')["home_yn"].sum()
            past_lst = set(dd_grp[dd_grp==48].index)
            inter_lst2 = list(today_lst.intersection(past_lst))

            if len(inter_lst2)>0:
                df3 = statcomm_tbl[statcomm_tbl.mbl_phon_num.isin(inter_lst2)].copy()
                df3["result"]=3
                statcomm_tbl.update(df3)

    ### result4 - 심각 (3일동안 power_off가 72번 발견되면 심각)
    mDF_tmp = mDF.copy()
    mDF_tmp["total_traffic"] = np.where((mDF.total_traffic>143),0,1)

    #Get the frequency of each phone number which total_traffic is equal to 0
    today_traffic_chk = mDF_tmp[mDF_tmp.total_traffic==1]
    today_traffic_chk = today_traffic_chk.groupby('mbl_phon_num')["total_traffic"].sum()
    today_lst = set(today_traffic_chk[today_traffic_chk==24].index)

    if len(today_lst)>0:
        '''Get the past frequency fo each phone number which total_traffic falled on 0'''
        tsql3 = "select * from datacomm where DATE_FORMAT(dt,'%Y%m%d') >= STR_TO_DATE('"+udt3_str+"','%Y-%m-%d')"
        mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
        traffic_past2 = pd.read_sql(tsql3,mariadb_connection_v2)

        if len(traffic_past2)>0:
            traffic_past2["total_traffic"] = np.where((traffic_past2.total_traffic>143),0,1)
            past_traffic_chk = traffic_past2[traffic_past2.total_traffic==1]
            past_traffic_chk = past_traffic_chk.groupby('mbl_phon_num')["total_traffic"].sum()
            past_lst = set(past_traffic_chk[past_traffic_chk==48].index)
            inter_lst3 = list(today_lst.intersection(past_lst))

            if len(inter_lst3)>0:
                df4 = statcomm_tbl[statcomm_tbl.mbl_phon_num.isin(inter_lst3)].copy()
                df4["result"]=4
                statcomm_tbl.update(df4)
    #
    # Write the two result files into csv's
    #
    '''1. write com_siteid_yyyymmdd_out_1.csv'''
    mDF["dt"] = mDF.dt.astype("str")
    mDF["dt"] = mDF.dt[0].replace("-","")
    mDF.to_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,8),sep=",",index=False)

    '''2. write com_siteid_yyyymmdd_out_2.csv'''
    statcomm_tbl["dt"] = statcomm_tbl.dt.astype("str")
    statcomm_tbl["dt"] = statcomm_tbl.dt[0].replace("-","")
    statcomm_tbl.to_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,9),sep=",",index=False)

    #
    # 5. 결과 DB 적재 (communication, home tables)
    #
    datacomm_tbl = pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,8),sep=",")
    statcomm_tbl = pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,9),sep=",")
   
    datacomm_tbl=datacomm_tbl.fillna(-999)
    statcomm_tbl=statcomm_tbl.fillna(-999)

    ddt = str(datacomm_tbl.dt[0])
    # print(ddt)

    mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
    utilfuncs.delData(ddt,datacomm_tbl,"datacomm",mariadb_connection_v2)
    s_cols = utilfuncs.get_cols(datacomm_tbl)
    utilfuncs.loadData_v2(datacomm_tbl,s_cols,"datacomm",mariadb_connection_v2)

    utilfuncs.delData(ddt,statcomm_tbl,"statcomm",mariadb_connection_v2)
    s_cols = utilfuncs.get_cols(statcomm_tbl)
    utilfuncs.loadData_v2(statcomm_tbl,s_cols,"statcomm",mariadb_connection_v2)
    
    # utilfuncs.delData(ddt,hometbl,"home",mariadb_connection)
    # utilfuncs.loadData(hometbl,"home",mariadb_connection)
    print("Type2 : JOB END")
    utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,f7,ymd,siteid)
    
else:
    utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,f7,ymd,siteid)

    print('ttt')
    def make_func
