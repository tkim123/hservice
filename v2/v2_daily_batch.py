#!/root/anaconda3/bin/python
'''service_site_info 테이블 읽어서 flag=Y인 지역에 대해서 코드 수행
'''
import sys,os
import pandas as pd
import numpy as np
import random
import collections
from tqdm import tqdm
import pickle
import itertools
import mysql.connector as mariadb
import multiprocessing
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
from v2_hpkg.v2_hservice_util2 import *

# def job(filenms):
#     return pd.read_csv(target_dir+filenms,engine="python",sep=",")

# p = multiprocessing.Pool(processes = multiprocessing.cpu_count()-1)
# battery_rack_lst = p.map(job, [i for i in battery_files])
# p.close()
# p.join()

# battery_rack = pd.concat(battery_rack_lst)
# battery_rack.shape

#
# READ service_site_info
con = mariadb.connect(user='skt', password='kepco123456/#',host="100.216.232.141",port="21152",database="oneperson")

tsql = "select * from SVC_AREA_DATA_USE_MNG"
stbl = pd.read_sql(tsql,con)
stbl = stbl.reset_index(drop="True")
stbl.rename(columns={"AREA_NO":"site","DATA_USE_YN":"flag"},inplace=True)

site_lst = list(stbl[stbl.flag=="Y"].site)

for sidx in site_lst:
    #check current date and time
    # now=datetime.now().strftime("%H:%M:%S")
    
    # print("="*20)
    # print("siteid: "+sidx+" Daily JOB START")

    ymd=date.today().strftime("%Y%m%d")
    # yy=int(utilfuncs.genFNM(sidx,ymd,1)[10:14])
    # mm=int(utilfuncs.genFNM(sidx,ymd,1)[14:16])
    # ddt=int(utilfuncs.genFNM(sidx,ymd,1)[16:18])
    # print("="*20)
    # print(ymd)
    # print(yy,mm,ddt)

    ## 00. delete log files by every the first day of a month
    if ymd[6:]=="01":
        os.system("rm -rf /root/cron.log")

    os.system("/root/hservice/v2/v2_code_template_0607.py "+ sidx)
