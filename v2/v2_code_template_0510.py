#!/root/anaconda3/bin/python
'''서비스 대상자별 시간대별 재실여부 판단 입력
    1. 서비스 대상자 자택 집주소의 위경도 (/shared/data/common/common_siteid_yyyymmdd.csv)
    2. 서비스 대상자의 시간대별 통화건수, SMS, 데이터 사용량, 위도, 경도 (셀기반) 
        (/shared/data/skt/skt_siteid_yyyymmdd_in_{1,2}.csv)
    3. 서비스 대상자의 시간대별 GPS (6분간격저장 (/shared/data/skt/other_siteid_yyyymmdd_in.csv))

결과 파일 저장 : /shared/data/skt/com_siteid_yyyymmdd_out.csv (휴대폰번호,일자,시간,통화건수,sms발신건수,데이터사용량,위도,경도,재실여부)
재실여부 : 0 재실, 1 외출
'''

import sys,os
import pandas as pd
import numpy as np
import random
import collections
from tqdm import tqdm
import pickle
import itertools
import mysql.connector as mariadb
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
from v2_hpkg.v2_hservice_util2 import *

#siteid="01"
siteid=sys.argv[1]
#siteid="0001"
print("="*25)
print("JOB START : site id = "+siteid)

# 0. 데이터분석 일자
ymd=date.today().strftime("%Y%m%d")
yy=int(utilfuncs.genFNM(siteid,ymd,1)[12:16])
mm=int(utilfuncs.genFNM(siteid,ymd,1)[16:18])
ddt=int(utilfuncs.genFNM(siteid,ymd,1)[18:20])

print(ymd)
print(yy,mm,ddt)

# 0. input file check
f1=utilfuncs.checkfile(siteid,ymd,1)
f2=utilfuncs.checkfile(siteid,ymd,2)
f3=utilfuncs.checkfile(siteid,ymd,3)
f4=utilfuncs.checkfile(siteid,ymd,4)
f5=utilfuncs.checkfile(siteid,ymd,5)
f6=utilfuncs.checkfile(siteid,ymd,6)
f7=utilfuncs.checkfile(siteid,ymd,7)
checkNum=f1+f2+f3+f4+f5+f6+f7
print(checkNum)

#if checkNum==7: #only consider the cae of checkNum=7, Nov-30-2020#
if f1==1 and f2==1 and f3==1 and f4==1 and f5==1 and f6==1:
    # 1. 서비스 대상자
    add_info=pd.read_csv("/shared/data/v2/user/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")
    add_info=add_info.rename(columns={"phone":"mbl_phon_num"})

    # 2. Read DT input files
    dt_info11=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,2),sep=",",header=None)
    dt_info12=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,3),sep=",",header=None)
    dt_info21=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,4),sep=",",header=None)
    dt_info22=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,5),sep=",",header=None)
    dt_info11.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info21.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info12.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]
    dt_info22.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]

    skt_input=pd.concat([dt_info11,dt_info21],axis=0)
    skt_feature=pd.concat([dt_info12,dt_info22],axis=0)

    # reformat hh
    hhlst=list(map(str,skt_feature.hh))
    hhlst2=[]
    for hdx in hhlst:
        if len(hdx)==1:
            hhlst2.append('0'+hdx)
        else:
            hhlst2.append(hdx)
    # modifed on 6-Aug
    # skt_feature.hh = pd.Series(hhlst2)
    skt_feature.hh = hhlst2
    skt_feature=skt_feature[skt_feature.hh!="\\N"]

    print(skt_input.dt.unique())
    print(skt_feature.dt.unique())

    # threshold=200
    # added 21-Aug, the result unit of utilfuncs.haversine is kilometer.
    # threshold = 0.2
    # Change threshold to 1km on 24-Aug-2020
    threshold = 1.0
    # yy=int(ymd[0:4])
    # mm=int(ymd[4:6])
    # ddt=int(ymd[6:8])
    skt_total=utilfuncs.Load_Data_SKT(skt_input,skt_feature,threshold,yy,mm,ddt)
    skt_total=skt_total[skt_total.latitude_nm!="cp"]
    skt_total=skt_total.reset_index(drop=True)

    # the following logic is removed on 26-Apr-2021.
    # (Remove the obersvation, the distance of which from the previous locaion is greater than 100 km)
    # # newly added on 7-Sep
    # phon_nms = skt_total.mbl_phon_num.unique()
    # phon_nms_len = len(phon_nms)

    # adjusted_df = pd.DataFrame()
    # for pidx in phon_nms:
    #     dist_df = skt_total[skt_total.mbl_phon_num==pidx]
    #     adjusted_dist_df = utilfuncs.adjust_dist(dist_df, 100)
    #     adjusted_df = pd.concat([adjusted_df, adjusted_dist_df], axis=0)

    # skt_total = adjusted_df.reset_index(drop=True)
    # # END 7-Sep

    #reformat phone number
    skt_total["phone_num"]=pd.Series(list(map(str,skt_total.mbl_phon_num)))
    skt_total["mbl_phon_num"]=skt_total.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
    skt_total=skt_total.drop(columns=["phone_num"])

    #reformat hh
    skt_total["hh"]=pd.Series(list(map(int,skt_total.hh)))

    # Final DT info 생성
    dt_info=skt_total[["dt","hh","mbl_phon_num","total_traffic","call_send_cnt","sms_send_cnt","latitude_nm","longitude_nm","home_yn"]]
    dt_info=dt_info.reset_index(drop=True)
    #dt_info=dt_info[dt_info.dt==(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")]
    #dt_info["dt"]=(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")
    dt_info["dt"]=pd.to_datetime(dt_info["dt"]).dt.date
    dt_info["home_yn"]=np.where(dt_info.home_yn=="Y",0,1)

    # 3. Read GPS Info
    gps_info=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,6),sep=",")
    '''newly added on 20-Nov due to the format change of gps info columns'''
    gps_info=gps_info[["phone","date","time","lat","lng"]]
    gpsDF=gps_info.rename(columns={"date":"dt","time":"hh","lat":"hlat","lng":"hlon","phone":"mbl_phon_num"})
    gpsDF["dt"]=(datetime(yy,mm,ddt)).strftime("%Y%m%d")
    gpsDF["dt"]=pd.to_datetime(gpsDF["dt"]).dt.date
  
    # merge fDF, add_info by phone number --> generate home_yn
    mDF=pd.merge(gpsDF,add_info[["mbl_phon_num","lat","lng"]],on="mbl_phon_num",how="left")
    mDF["home_distance"]=mDF[["hlat","hlon","lat","lng"]].apply(lambda row:utilfuncs.calcDist(row["hlat"],row["hlon"],row["lat"],row["lng"]),axis=1)
    mDF["home_yn"]=np.where(mDF.home_distance<100,0,1)
    mDF=mDF.rename(columns={"phone":"mbl_phon_num","lat":"home_lat","lng":"home_lon","hlat":"latitude_nm","hlon":"longitude_nm"})
    # Reorder columns
    mDF=mDF[["hh","dt","mbl_phon_num","latitude_nm","longitude_nm","home_yn"]]

    # 4. 결과 csv 파일 생성
    s1=set(add_info.mbl_phon_num.unique())
    s2=set(dt_info.mbl_phon_num.unique())
    s3=set(mDF.mbl_phon_num.unique())

    # select K1 class (add_info, skt_info Not in GPS)
    k1=(s1.intersection(s2)).difference(s3)
    k1DF=dt_info[dt_info["mbl_phon_num"].isin(k1)]

    # select K2 class (add_info, skt_info, GPS)
    k2=(s1.intersection(s2)).intersection(s3)
    tmp1=dt_info[dt_info["mbl_phon_num"].isin(k2)]
    tmp1=tmp1.drop(columns=["latitude_nm","longitude_nm","home_yn"])
    tmp2=mDF[mDF["mbl_phon_num"].isin(k2)]

    k2DF=pd.merge(tmp2,tmp1,on=["dt","hh","mbl_phon_num"],how="left")
    
    # select K3 class (add_info, GPS Not in skt_info)
    k3=(s1.intersection(s3).difference(s2))
    k3DF=mDF[mDF["mbl_phon_num"].isin(k3)]

    # Exclude k1 class in the result --> newly added on 30-Jan-2020
    # mDF=pd.concat([k1DF,k2DF,k3DF],axis=0,sort=False)

    # Inclue K1 class in the result --> newly added on 19-June-2020
    # mDF=pd.concat([k2DF,k3DF],axis=0,sort=False)
    mDF=pd.concat([k1DF,k2DF,k3DF],axis=0,sort=False)
    mDF = mDF.reset_index(drop=True) # 31-Mar-2021 수정
    mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn"]]
    mDF=mDF.sort_values(by=["mbl_phon_num","hh"])
    # convert GB to KB
    mDF["total_traffic"] = mDF["total_traffic"].astype("float")*1000000

    # non-skt customers' mno into added -- on 21-Apr-2020
    if f7==1:
        notskt=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,7),sep=",",header=None)
        notskt.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]
        notskt=notskt.reset_index(drop=True)

        #reformat columns
        #notskt=utilfuncs.reformat_hh(notskt)
        
        notskt.dt=pd.Series(list(map(str,notskt.dt)))
        notskt["dt"]=pd.to_datetime(notskt["dt"]).dt.date

        #reformat phone number
        notskt["phone_num"]=pd.Series(list(map(str,notskt.mbl_phon_num)))
        notskt["mbl_phon_num"]=notskt.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
        notskt=notskt.drop(columns=["phone_num"])

        #update mDF'mno columns with  notskt's
        a1=mDF.set_index(["mbl_phon_num","dt","hh"])
        a2=notskt.set_index(["mbl_phon_num","dt","hh"])
        a1.update(a2)
        mDF=a1.reset_index()
        #added on 17-May-2021 reset_index 후 dt 컬럼이 timestamp로 바뀌게 된다. 따라서 다시 datetime 형식으로 변환
        mDF["dt"] = pd.to_datetime(mDF["dt"]).dt.date

    #
    # create the "power off" column
    # mDF["power_off"] = np.where((mDF.total_traffic==0),0,1)
    # mDF["power_off"] = np.where((mDF.total_traffic==0),1,0)  # 1-Feb-21 수정
    
    # 10-May-2021 수정 --> 위치 정보는 없고 features정보만 있는 휴대폰 번호가 power-off임
    mDF["power_off"] = 0
    loc_phon_num = set(skt_input.mbl_phon_num.unique())
    feat_phon_num = set(skt_feature.mbl_phon_num.unique())
    diff_num = feat_phon_num.difference(loc_phon_num)

    if len(diff_num)>0:
        feat_dt = datetime.strftime(datetime.strptime(ymd, "%Y%m%d") - timedelta(days=1),"%Y%m%d")
        tmp_df = skt_feature[skt_feature.mbl_phon_num.isin(list(diff_num)) & (skt_feature.dt==feat_dt)]
        tmp_df = tmp_df.reset_index(drop=True)
        tmp_df["power_off"] = 1

        #reformat phone number
        tmp_df["phone_num"]=pd.Series(list(map(str,tmp_df.mbl_phon_num)))
        tmp_df["mbl_phon_num"]=tmp_df.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
        tmp_df=tmp_df.drop(columns=["phone_num"])

        #convert dt & total_traffic
        tmp_df["dt"]=pd.to_datetime(tmp_df["dt"]).dt.date
        tmp_df["total_traffic"] = tmp_df["total_traffic"].astype("float")*1000000

        # total_traffic,통화건수,문자발신건수 도 완전히 0 이어야지만 power_off로 판단함 17-May-2021
        # tmp_df["power_off"] = np.where((tmp_df.total_traffic==0), 1, 0)
        tmp_df["power_off"] = np.where(((tmp_df.sms_send_cnt==0) & (tmp_df.call_send_cnt==0) & (tmp_df.total_traffic==0)),1,0)
        
        # tmp_df, mDF concatenate
        mDF = pd.concat([mDF, tmp_df],axis=0,sort=True)
        mDF = mDF.reset_index(drop=True)
        mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn","power_off"]]
        mDF=mDF.sort_values(by=["mbl_phon_num","hh"])

    # create statcomm_tbl
    # result0 - 정상, 1 - 관심, 2 - 주의, 3 - 경계, 4 - 심각
    statcomm_tbl = mDF[["mbl_phon_num","dt"]].drop_duplicates()
    statcomm_tbl = statcomm_tbl.reset_index(drop=True)
    statcomm_tbl["result"] = 0

    ### result1 - 관심
    mDF_tmp = mDF.copy()
    mDF_tmp["total_traffic"] = np.where((mDF.total_traffic>143),mDF.total_traffic,0)
    mDF_tmp1 = mDF_tmp.groupby("mbl_phon_num")["call_send_cnt","sms_send_cnt","total_traffic","home_yn"].sum()
    mDF_tmp1["tot_sum"] = mDF_tmp1.sum(axis=1)
    result1_lst = mDF_tmp1[mDF_tmp1.tot_sum==0].index
    statcomm_tbl["result"] = np.where((statcomm_tbl.mbl_phon_num.isin(result1_lst)),1,statcomm_tbl.result)

    ### result2 - 주의 (2일 연속 관심)
    udt = mDF.dt[0]
    udt2 = udt - timedelta(days=1)
    udt3 = udt - timedelta(days=2)

    udt_str = datetime.strftime(udt,"%Y-%m-%d")
    udt2_str = datetime.strftime(udt2,"%Y-%m-%d")
    udt3_str = datetime.strftime(udt3,"%Y-%m-%d")

    # select yesterday - data from stacomm
    tsql = "select * from statcomm where DATE_FORMAT(dt,'%Y%m%d') = STR_TO_DATE('"+udt2_str +"','%Y-%m-%d')"
    mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
    statcomm_yester = pd.read_sql(tsql,mariadb_connection_v2)
    statcomm_yester_lst = set(statcomm_yester[statcomm_yester.result==1].mbl_phon_num)
    statcomm_today_lst = set(statcomm_tbl[statcomm_tbl.result==1].mbl_phon_num)

    inter_lst = list(statcomm_yester_lst.intersection(statcomm_today_lst))
    if len(inter_lst)>0:
        df2 = statcomm_tbl[statcomm_tbl.mbl_phon_num.isin(inter_lst)].copy()
        df2["result"]=2
        statcomm_tbl.update(df2)

    ### result3 - 경계 (3일동안 장기 외출의 경우)
    mDF_tmp2 = mDF.groupby('mbl_phon_num')["home_yn"].sum()
    today_lst = set(mDF_tmp2[mDF_tmp2==24].index)

    if len(today_lst)>0:
    # select the values of home_yn during past 24 hours
        tsql2 = "select * from datacomm where DATE_FORMAT(dt,'%Y%m%d') >= STR_TO_DATE('"+udt3_str+"','%Y-%m-%d') and DATE_FORMAT(dt,'%Y%m%d') <= STR_TO_DATE('"+udt2_str+"','%Y-%m-%d')"
        mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
        datacomm_past2 = pd.read_sql(tsql2,mariadb_connection_v2)

        if len(datacomm_past2)>0:
            dd_grp = datacomm_past2.groupby('mbl_phon_num')["home_yn"].sum()
            past_lst = set(dd_grp[dd_grp==48].index)
            inter_lst2 = list(today_lst.intersection(past_lst))

            if len(inter_lst2)>0:
                df3 = statcomm_tbl[statcomm_tbl.mbl_phon_num.isin(inter_lst2)].copy()
                df3["result"]=3
                statcomm_tbl.update(df3)

    # ### result4 - 심각 (3일동안 power_off가 72번 발견되면 심각)
    # mDF_tmp = mDF.copy()
    # mDF_tmp["total_traffic"] = np.where((mDF.total_traffic>0),0,1)

    # #Get the frequency of each phone number which total_traffic is equal to 0
    # today_traffic_chk = mDF_tmp[mDF_tmp.total_traffic==1]
    # today_traffic_chk = today_traffic_chk.groupby('mbl_phon_num')["total_traffic"].sum()
    # today_lst = set(today_traffic_chk[today_traffic_chk==24].index)

    # if len(today_lst)>0:
    #     '''Get the past frequency fo each phone number which total_traffic falled on 0'''
    #     tsql3 = "select * from datacomm where DATE_FORMAT(dt,'%Y%m%d') >= STR_TO_DATE('"+udt3_str+"','%Y-%m-%d') and DATE_FORMAT(dt,'%Y%m%d') <= STR_TO_DATE('"+udt2_str+"','%Y-%m-%d')"
    #     mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
    #     traffic_past2 = pd.read_sql(tsql3,mariadb_connection_v2)

    #     if len(traffic_past2)>0:
    #         traffic_past2["total_traffic"] = np.where((traffic_past2.total_traffic>0),0,1)
    #         past_traffic_chk = traffic_past2[traffic_past2.total_traffic==1]
    #         past_traffic_chk = past_traffic_chk.groupby('mbl_phon_num')["total_traffic"].sum()
    #         past_lst = set(past_traffic_chk[past_traffic_chk==48].index)
    #         inter_lst3 = list(today_lst.intersection(past_lst))

    #         if len(inter_lst3)>0:
    #             df4 = statcomm_tbl[statcomm_tbl.mbl_phon_num.isin(inter_lst3)].copy()
    #             df4["result"]=4
    #             statcomm_tbl.update(df4)

    ### result4 - 심각 (3일동안 power_off가 72번, 통화수발신=0 이 72번, 문자발신=0 이 72번 발견되면 심각)
    mDF_tmp = mDF.copy()
    mDF_tmp["total_traffic"] = np.where((mDF.total_traffic>0),0,1)
    mDF_tmp["call_send_cnt"] = np.where((mDF.call_send_cnt>0),0,1)
    mDF_tmp["sms_send_cnt"] = np.where((mDF.sms_send_cnt>0),0,1)

    # Result 4 is aplied for only SKT customers. --> modified on 23-Feb-2021
    skt_lst = list(add_info[add_info.telecom=="SKT"].mbl_phon_num)
    mDF_tmp = mDF_tmp[mDF_tmp.mbl_phon_num.isin(skt_lst)]

    #Get the frequency of each phone number which total_traffic is equal to 0
    today_traffic_chk = mDF_tmp[mDF_tmp.total_traffic==1]
    today_traffic_chk = today_traffic_chk.groupby('mbl_phon_num')["call_send_cnt","sms_send_cnt","total_traffic"].sum()
    #today_lst = set(today_traffic_chk[today_traffic_chk==24].index)
    today_lst = set(today_traffic_chk[(today_traffic_chk.call_send_cnt==24) & (today_traffic_chk.sms_send_cnt==24) & (today_traffic_chk.total_traffic==24)].index)

    if len(today_lst)>0:
        '''Get the past frequency fo each phone number which total_traffic falled on 0'''
        tsql3 = "select * from datacomm where DATE_FORMAT(dt,'%Y%m%d') >= STR_TO_DATE('"+udt3_str+"','%Y-%m-%d') and DATE_FORMAT(dt,'%Y%m%d') <= STR_TO_DATE('"+udt2_str+"','%Y-%m-%d')"
        mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
        traffic_past2 = pd.read_sql(tsql3,mariadb_connection_v2)

        if len(traffic_past2)>0:
            traffic_past2["total_traffic"] = np.where((traffic_past2.total_traffic>0),0,1)
            traffic_past2["call_send_cnt"] = np.where((traffic_past2.call_send_cnt>0),0,1)
            traffic_past2["sms_send_cnt"] = np.where((traffic_past2.sms_send_cnt>0),0,1)
            past_traffic_chk = traffic_past2[traffic_past2.total_traffic==1]
            past_traffic_chk = past_traffic_chk.groupby('mbl_phon_num')["call_send_cnt","sms_send_cnt","total_traffic"].sum()
            past_lst = set(past_traffic_chk[(past_traffic_chk.call_send_cnt==48) & (past_traffic_chk.sms_send_cnt==48) & (past_traffic_chk.total_traffic==48)].index)
            inter_lst3 = list(today_lst.intersection(past_lst))

            if len(inter_lst3)>0:
                df4 = statcomm_tbl[statcomm_tbl.mbl_phon_num.isin(inter_lst3)].copy()
                df4["result"]=4
                statcomm_tbl.update(df4)

    #
    # Write the two result files into csv's
    #
    '''1. write com_siteid_yyyymmdd_out_1.csv'''
    mDF["dt"] = mDF.dt.astype("str")
    mDF["dt"] = mDF.dt[0].replace("-","")
    mDF.to_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,8),sep=",",index=False)

    '''2. write com_siteid_yyyymmdd_out_2.csv'''
    statcomm_tbl["dt"] = statcomm_tbl.dt.astype("str")
    statcomm_tbl["dt"] = statcomm_tbl.dt[0].replace("-","")
    statcomm_tbl.to_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,9),sep=",",index=False)

    #
    # 5. 결과 DB 적재 (communication, home tables)
    #
    datacomm_tbl = pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,8),sep=",")
    statcomm_tbl = pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,9),sep=",")
   
    datacomm_tbl=datacomm_tbl.fillna(-999)
    statcomm_tbl=statcomm_tbl.fillna(-999)

    ddt = str(datacomm_tbl.dt[0])
    # print(ddt)

    mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
    utilfuncs.delData(ddt,datacomm_tbl,"datacomm",mariadb_connection_v2)
    s_cols = utilfuncs.get_cols(datacomm_tbl)
    utilfuncs.loadData_v2(datacomm_tbl,s_cols,"datacomm",mariadb_connection_v2)

    utilfuncs.delData(ddt,statcomm_tbl,"statcomm",mariadb_connection_v2)
    s_cols = utilfuncs.get_cols(statcomm_tbl)
    utilfuncs.loadData_v2(statcomm_tbl,s_cols,"statcomm",mariadb_connection_v2)
    
    # utilfuncs.delData(ddt,hometbl,"home",mariadb_connection)
    # utilfuncs.loadData(hometbl,"home",mariadb_connection)
    print("Type1 : JOB END")
    utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,f7,ymd,siteid)

    # # # # 적재 확인
    # # make columns (for the purpose of inserting)
    # def get_cols(dfnm):
    #     s_cols="("
    #     for i in range(len(dfnm.columns)):
    #         if i < len(dfnm.columns)-1:
    #             s_cols = s_cols+dfnm.columns[i]+","
    #         else:
    #             s_cols = s_cols+dfnm.columns[i]+")"
    #     return s_cols
    
    # mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
    # tsql="select * from datacomm"
    # tsql2 = "select * from statcomm"
    # test=pd.read_sql(tsql,mariadb_connection_v2)
    # test2=pd.read_sql(tsql2,mariadb_connection_v2)
    # tsql1="select * from home"
    # home=pd.read_sql(tsql1,mariadb_connection)

    # mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
    # pd.read_sql("select count(*) from communication where measurementDate='2019-11-22'",mariadb_connection)

# GPS정보가 전송되지 않았을 경우(다른 5개 파일은 전송됨)
elif f1==1 and f2==1 and f3==1 and f4==1 and f5==1 and f6==0:
    '''only skt information is available'''
        # 1. 서비스 대상자
    add_info=pd.read_csv("/shared/data/v2/user/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")
    add_info=add_info.rename(columns={"phone":"mbl_phon_num"})

    # 2. Read DT input files
    dt_info11=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,2),sep=",",header=None)
    dt_info12=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,3),sep=",",header=None)
    dt_info21=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,4),sep=",",header=None)
    dt_info22=pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,5),sep=",",header=None)
    dt_info11.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info21.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info12.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]
    dt_info22.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]

    skt_input=pd.concat([dt_info11,dt_info21],axis=0)
    skt_feature=pd.concat([dt_info12,dt_info22],axis=0)

    # reformat hh
    hhlst=list(map(str,skt_feature.hh))
    hhlst2=[]
    for hdx in hhlst:
        if len(hdx)==1:
            hhlst2.append('0'+hdx)
        else:
            hhlst2.append(hdx)
    # modifed on 6-Aug
    # skt_feature.hh = pd.Series(hhlst2)
    skt_feature.hh = hhlst2
    skt_feature=skt_feature[skt_feature.hh!="\\N"]

    print(skt_input.dt.unique())
    print(skt_feature.dt.unique())

    # threshold=200
    # added 21-Aug, the result unit of utilfuncs.haversine is kilometer.
    # threshold = 0.2
    # Change threshold to 1km on 24-Aug-2020
    threshold = 1.0
    # yy=int(ymd[0:4])
    # mm=int(ymd[4:6])
    # ddt=int(ymd[6:8])
    skt_total=utilfuncs.Load_Data_SKT(skt_input,skt_feature,threshold,yy,mm,ddt)
    skt_total=skt_total[skt_total.latitude_nm!="cp"]
    skt_total=skt_total.reset_index(drop=True)

    # the following logic is removed on 26-Apr-2021.
    # (Remove the obersvation, the distance of which from the previous locaion is greater than 100 km)
    # # newly added on 7-Sep
    # phon_nms = skt_total.mbl_phon_num.unique()
    # phon_nms_len = len(phon_nms)

    # adjusted_df = pd.DataFrame()
    # for pidx in phon_nms:
    #     dist_df = skt_total[skt_total.mbl_phon_num==pidx]
    #     adjusted_dist_df = utilfuncs.adjust_dist(dist_df, 100)
    #     adjusted_df = pd.concat([adjusted_df, adjusted_dist_df], axis=0)

    # skt_total = adjusted_df.reset_index(drop=True)
    # # END 7-Sep

    #reformat phone number
    skt_total["phone_num"]=pd.Series(list(map(str,skt_total.mbl_phon_num)))
    skt_total["mbl_phon_num"]=skt_total.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
    skt_total=skt_total.drop(columns=["phone_num"])

    #reformat hh
    skt_total["hh"]=pd.Series(list(map(int,skt_total.hh)))

    # Final DT info 생성
    dt_info=skt_total[["dt","hh","mbl_phon_num","total_traffic","call_send_cnt","sms_send_cnt","latitude_nm","longitude_nm","home_yn"]]
    dt_info=dt_info.reset_index(drop=True)
    #dt_info=dt_info[dt_info.dt==(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")]
    #dt_info["dt"]=(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")
    dt_info["dt"]=pd.to_datetime(dt_info["dt"]).dt.date
    dt_info["home_yn"]=np.where(dt_info.home_yn=="Y",0,1)

    # 3. Read GPS Info
    # --> No GPS Info is available.

    # 4. 결과 csv 파일 생성
    s1=set(add_info.mbl_phon_num.unique())
    s2=set(dt_info.mbl_phon_num.unique())

    # select K1 class (add_info, skt_info)
    k1=s1.intersection(s2)
    k1DF=dt_info[dt_info["mbl_phon_num"].isin(k1)]
    mDF = k1DF
    mDF = mDF.reset_index(drop=True) # 31-Mar-2021 수정

    mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn"]]
    mDF=mDF.sort_values(by=["mbl_phon_num","hh"])
    # convert GB to KB
    mDF["total_traffic"] = mDF["total_traffic"].astype("float")*1000000

    #
    # create the "power off" column
    # mDF["power_off"] = np.where((mDF.total_traffic==0),0,1)
    # mDF["power_off"] = np.where((mDF.total_traffic==0),1,0)  # 1-Feb-21 수정
    
    # 10-May-2021 수정 --> 위치 정보는 없고 features정보만 있는 휴대폰 번호가 power-off임
    mDF["power_off"] = 0
    loc_phon_num = set(skt_input.mbl_phon_num.unique())
    feat_phon_num = set(skt_feature.mbl_phon_num.unique())
    diff_num = feat_phon_num.difference(loc_phon_num)

    if len(diff_num)>0:
        feat_dt = datetime.strftime(datetime.strptime(ymd, "%Y%m%d") - timedelta(days=1),"%Y%m%d")
        tmp_df = skt_feature[skt_feature.mbl_phon_num.isin(list(diff_num)) & (skt_feature.dt==feat_dt)]
        tmp_df = tmp_df.reset_index(drop=True)
        tmp_df["power_off"] = 1

        #reformat phone number
        tmp_df["phone_num"]=pd.Series(list(map(str,tmp_df.mbl_phon_num)))
        tmp_df["mbl_phon_num"]=tmp_df.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
        tmp_df=tmp_df.drop(columns=["phone_num"])

        #convert dt & total_traffic
        tmp_df["dt"]=pd.to_datetime(tmp_df["dt"]).dt.date
        tmp_df["total_traffic"] = tmp_df["total_traffic"].astype("float")*1000000

        # total_traffic,통화건수,문자발신건수 도 완전히 0 이어야지만 power_off로 판단함 17-May-2021
        # tmp_df["power_off"] = np.where((tmp_df.total_traffic==0), 1, 0)
        tmp_df["power_off"] = np.where(((tmp_df.sms_send_cnt==0) & (tmp_df.call_send_cnt==0) & (tmp_df.total_traffic==0)),1,0)
        
        # tmp_df, mDF concatenate
        mDF = pd.concat([mDF, tmp_df],axis=0,sort=True)
        mDF = mDF.reset_index(drop=True)
        mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn","power_off"]]
        mDF=mDF.sort_values(by=["mbl_phon_num","hh"])
        
    # create statcomm_tbl
    # result0 - 정상, 1 - 관심, 2 - 주의, 3 - 경계, 4 - 심각
    statcomm_tbl = mDF[["mbl_phon_num","dt"]].drop_duplicates()
    statcomm_tbl = statcomm_tbl.reset_index(drop=True)
    statcomm_tbl["result"] = 0

    ### result1 - 관심
    mDF_tmp = mDF.copy()
    mDF_tmp["total_traffic"] = np.where((mDF.total_traffic>143),mDF.total_traffic,0)
    mDF_tmp1 = mDF_tmp.groupby("mbl_phon_num")["call_send_cnt","sms_send_cnt","total_traffic","home_yn"].sum()
    mDF_tmp1["tot_sum"] = mDF_tmp1.sum(axis=1)
    result1_lst = mDF_tmp1[mDF_tmp1.tot_sum==0].index
    statcomm_tbl["result"] = np.where((statcomm_tbl.mbl_phon_num.isin(result1_lst)),1,statcomm_tbl.result)

    ### result2 - 주의 (2일 연속 관심)
    udt = mDF.dt[0]
    udt2 = udt - timedelta(days=1)
    udt3 = udt - timedelta(days=2)

    udt_str = datetime.strftime(udt,"%Y-%m-%d")
    udt2_str = datetime.strftime(udt2,"%Y-%m-%d")
    udt3_str = datetime.strftime(udt3,"%Y-%m-%d")

    # select yesterday - data from stacomm
    tsql = "select * from statcomm where DATE_FORMAT(dt,'%Y%m%d') = STR_TO_DATE('"+udt2_str +"','%Y-%m-%d')"
    mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
    statcomm_yester = pd.read_sql(tsql,mariadb_connection_v2)
    statcomm_yester_lst = set(statcomm_yester[statcomm_yester.result==1].mbl_phon_num)
    statcomm_today_lst = set(statcomm_tbl[statcomm_tbl.result==1].mbl_phon_num)

    inter_lst = list(statcomm_yester_lst.intersection(statcomm_today_lst))
    if len(inter_lst)>0:
        df2 = statcomm_tbl[statcomm_tbl.mbl_phon_num.isin(inter_lst)].copy()
        df2["result"]=2
        statcomm_tbl.update(df2)

    ### result3 - 경계 (3일동안 장기 외출의 경우)
    mDF_tmp2 = mDF.groupby('mbl_phon_num')["home_yn"].sum()
    today_lst = set(mDF_tmp2[mDF_tmp2==24].index)

    if len(today_lst)>0:
    # select the values of home_yn during past 24 hours
        tsql2 = "select * from datacomm where DATE_FORMAT(dt,'%Y%m%d') >= STR_TO_DATE('"+udt3_str+"','%Y-%m-%d') and DATE_FORMAT(dt,'%Y%m%d') <= STR_TO_DATE('"+udt2_str+"','%Y-%m-%d')"
        mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
        datacomm_past2 = pd.read_sql(tsql2,mariadb_connection_v2)

        if len(datacomm_past2)>0:
            dd_grp = datacomm_past2.groupby('mbl_phon_num')["home_yn"].sum()
            past_lst = set(dd_grp[dd_grp==48].index)
            inter_lst2 = list(today_lst.intersection(past_lst))

            if len(inter_lst2)>0:
                df3 = statcomm_tbl[statcomm_tbl.mbl_phon_num.isin(inter_lst2)].copy()
                df3["result"]=3
                statcomm_tbl.update(df3)

    ### result4 - 심각 (3일동안 power_off가 72번, 통화수발신=0 이 72번, 문자발신=0 이 72번 발견되면 심각)
    mDF_tmp = mDF.copy()
    mDF_tmp["total_traffic"] = np.where((mDF.total_traffic>0),0,1)
    mDF_tmp["call_send_cnt"] = np.where((mDF.call_send_cnt>0),0,1)
    mDF_tmp["sms_send_cnt"] = np.where((mDF.sms_send_cnt>0),0,1)

    # Result 4 is aplied for only SKT customers. --> modified on 23-Feb-2021
    skt_lst = list(add_info[add_info.telecom=="SKT"].mbl_phon_num)
    mDF_tmp = mDF_tmp[mDF_tmp.mbl_phon_num.isin(skt_lst)]

    #Get the frequency of each phone number which total_traffic is equal to 0
    today_traffic_chk = mDF_tmp[mDF_tmp.total_traffic==1]
    today_traffic_chk = today_traffic_chk.groupby('mbl_phon_num')["call_send_cnt","sms_send_cnt","total_traffic"].sum()
    #today_lst = set(today_traffic_chk[today_traffic_chk==24].index)
    today_lst = set(today_traffic_chk[(today_traffic_chk.call_send_cnt==24) & (today_traffic_chk.sms_send_cnt==24) & (today_traffic_chk.total_traffic==24)].index)

    if len(today_lst)>0:
        '''Get the past frequency fo each phone number which total_traffic falled on 0'''
        tsql3 = "select * from datacomm where DATE_FORMAT(dt,'%Y%m%d') >= STR_TO_DATE('"+udt3_str+"','%Y-%m-%d') and DATE_FORMAT(dt,'%Y%m%d') <= STR_TO_DATE('"+udt2_str+"','%Y-%m-%d')"
        mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
        traffic_past2 = pd.read_sql(tsql3,mariadb_connection_v2)

        if len(traffic_past2)>0:
            traffic_past2["total_traffic"] = np.where((traffic_past2.total_traffic>0),0,1)
            traffic_past2["call_send_cnt"] = np.where((traffic_past2.call_send_cnt>0),0,1)
            traffic_past2["sms_send_cnt"] = np.where((traffic_past2.sms_send_cnt>0),0,1)
            past_traffic_chk = traffic_past2[traffic_past2.total_traffic==1]
            past_traffic_chk = past_traffic_chk.groupby('mbl_phon_num')["call_send_cnt","sms_send_cnt","total_traffic"].sum()
            past_lst = set(past_traffic_chk[(past_traffic_chk.call_send_cnt==48) & (past_traffic_chk.sms_send_cnt==48) & (past_traffic_chk.total_traffic==48)].index)
            inter_lst3 = list(today_lst.intersection(past_lst))

            if len(inter_lst3)>0:
                df4 = statcomm_tbl[statcomm_tbl.mbl_phon_num.isin(inter_lst3)].copy()
                df4["result"]=4
                statcomm_tbl.update(df4)
    #
    # Write the two result files into csv's
    #
    '''1. write com_siteid_yyyymmdd_out_1.csv'''
    mDF["dt"] = mDF.dt.astype("str")
    mDF["dt"] = mDF.dt[0].replace("-","")
    mDF.to_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,8),sep=",",index=False)

    '''2. write com_siteid_yyyymmdd_out_2.csv'''
    statcomm_tbl["dt"] = statcomm_tbl.dt.astype("str")
    statcomm_tbl["dt"] = statcomm_tbl.dt[0].replace("-","")
    statcomm_tbl.to_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,9),sep=",",index=False)

    #
    # 5. 결과 DB 적재 (communication, home tables)
    #
    datacomm_tbl = pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,8),sep=",")
    statcomm_tbl = pd.read_csv("/shared/data/v2/com/"+utilfuncs.genFNM(siteid,ymd,9),sep=",")
   
    datacomm_tbl=datacomm_tbl.fillna(-999)
    statcomm_tbl=statcomm_tbl.fillna(-999)

    ddt = str(datacomm_tbl.dt[0])
    # print(ddt)

    mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
    utilfuncs.delData(ddt,datacomm_tbl,"datacomm",mariadb_connection_v2)
    s_cols = utilfuncs.get_cols(datacomm_tbl)
    utilfuncs.loadData_v2(datacomm_tbl,s_cols,"datacomm",mariadb_connection_v2)

    utilfuncs.delData(ddt,statcomm_tbl,"statcomm",mariadb_connection_v2)
    s_cols = utilfuncs.get_cols(statcomm_tbl)
    utilfuncs.loadData_v2(statcomm_tbl,s_cols,"statcomm",mariadb_connection_v2)
    
    # utilfuncs.delData(ddt,hometbl,"home",mariadb_connection)
    # utilfuncs.loadData(hometbl,"home",mariadb_connection)
    print("Type2 : JOB END")
    utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,f7,ymd,siteid)
    
else:
    utilfuncs.printMsg(f1,f2,f3,f4,f5,f6,f7,ymd,siteid)


#  # # # # 적재 확인
# # make columns (for the purpose of inserting)
# def get_cols(dfnm):
#     s_cols="("
#     for i in range(len(dfnm.columns)):
#         if i < len(dfnm.columns)-1:
#             s_cols = s_cols+dfnm.columns[i]+","
#         else:
#             s_cols = s_cols+dfnm.columns[i]+")"
#     return s_cols

# mariadb_connection_v2 = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="oneperson")
# tsql="select * from datacomm"
# tsql2 = "select * from statcomm"
# test=pd.read_sql(tsql,mariadb_connection_v2)
# test2=pd.read_sql(tsql2,mariadb_connection_v2)
# tsql1="select * from home"
# home=pd.read_sql(tsql1,mariadb_connection)
