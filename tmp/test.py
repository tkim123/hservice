# DT의 2개 파일이 모두 전송되지 않았을 경우 (GPS정보는 전송 되어야 함)
if f1==1 and f2==0 and f3==0 and f6==1:
    # 1. 서비스 대상자
    add_info=pd.read_csv("/shared/data/common/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")
    add_info=add_info.rename(columns={"phone":"mbl_phon_num"})

    # 2. Can't Read DT input files

    # 3. Read GPS Info
    gps_info=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,6),sep=",")
    '''newly added on 20-Nov due to the format change of gps info columns'''
    gps_info=gps_info[["phone","date","time","lat","lng"]]
    gpsDF=gps_info.rename(columns={"date":"dt","time":"hh","lat":"hlat","lng":"hlon","phone":"mbl_phon_num"})
    gpsDF["dt"]=(datetime(yy,mm,ddt)).strftime("%Y%m%d")
    gpsDF["dt"]=pd.to_datetime(gpsDF["dt"]).dt.date
    
    # merge fDF, add_info by phone number --> generate home_yn
    mDF=pd.merge(gpsDF,add_info[["mbl_phon_num","lat","lng"]],on="mbl_phon_num",how="left")
    mDF["home_distance"]=mDF[["hlat","hlon","lat","lng"]].apply(lambda row:utilfuncs.calcDist(row["hlat"],row["hlon"],row["lat"],row["lng"]),axis=1)
    mDF["home_yn"]=np.where(mDF.home_distance<100,0,1)
    mDF=mDF.rename(columns={"phone":"mbl_phon_num","lat":"home_lat","lng":"home_lon","hlat":"latitude_nm","hlon":"longitude_nm"})
    # Reorder columns
    mDF=mDF[["hh","dt","mbl_phon_num","latitude_nm","longitude_nm","home_yn"]]

    # 4. 결과 csv 파일 생성
    s1=set(add_info.mbl_phon_num.unique())
    s3=set(mDF.mbl_phon_num.unique())
    
    # select K5 class (add_info, GPS)
    k5=s1.intersection(s3)
    k5DF=mDF[mDF["mbl_phon_num"].isin(k5)]

    mDF=k5DF
    mDF["call_send_cnt"]=-999
    mDF["sms_send_cnt"]=-999
    mDF["total_traffic"]=-999
    mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn"]]
    mDF=mDF.sort_values(by=["mbl_phon_num","hh"])
    mDF.to_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",",index=False )

    # 5. 결과 DB 적재 (communication, home tables)
    mDF=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",")
    mDF["siteID"]=siteid
    ddt=mDF.dt.unique()[0]
    print(ddt, len(mDF.dt.unique()))

    commutbl=mDF[["mbl_phon_num","hh","latitude_nm","longitude_nm","call_send_cnt","sms_send_cnt","total_traffic","dt","siteID"]]
    commutbl=commutbl.rename(columns={"mbl_phon_num":"phone","hh":"time","latitude_nm":"lat","longitude_nm":"lng","call_send_cnt":"call","sms_send_cnt":"sms","total_traffic":"data","dt":"measurementDate"})
    commutbl=commutbl.fillna(-999)

    hometbl=mDF[["mbl_phon_num","hh","home_yn","dt","siteID"]]
    hometbl=hometbl.rename(columns={"mbl_phon_num":"phone","hh":"time","home_yn":"home","dt":"measurementDate"})

    mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
    utilfuncs.delData(ddt,commutbl,"communication",mariadb_connection)
    utilfuncs.loadData(commutbl,"communication",mariadb_connection)
    utilfuncs.delData(ddt,hometbl,"home",mariadb_connection)
    utilfuncs.loadData(hometbl,"home",mariadb_connection)
    print("Type5 : JOB END")


def printMsg(f1,f2,f3,f4,f5,f6):
    print("check input files")
    if f1==0:
        print("check "+utilfuncs.genFNM(siteid,ymd,1)+" !!!")
    if f2==0:
        print("check "+utilfuncs.genFNM(siteid,ymd,2)+" !!!")
    if f3==0:
        print("check "+utilfuncs.genFNM(siteid,ymd,3)+" !!!")
    if f4==0:
        print("check "+utilfuncs.genFNM(siteid,ymd,4)+" !!!")
    if f5==0:
        print("check "+utilfuncs.genFNM(siteid,ymd,5)+" !!!")
    if f6==0:
        print("check "+utilfuncs.genFNM(siteid,ymd,6)+" !!!")
printMsg(f1,f2,f3,f4,f5,f6)
