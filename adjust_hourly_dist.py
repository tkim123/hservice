phon_nms = skt_total.mbl_phon_num.unique()

adjusted_df = pd.DataFrame()
for pidx in phon_nms:
    dist_df = skt_total[skt_total.mbl_phon_num==pidx]
    adjusted_dist_df = adjust_dist(dist_df, 100)
    adjusted_df = pd.concat([adjusted_df, adjusted_dist_df], axis=0)
    


def adjust_dist_old(dist_df, dist_limit):
    '''remove observations, the distance difference between two consecutive locations is greater than 100 km.'''
    dist_df = dist_df.sort_values("hh")
    dist_df = dist_df.reset_index(drop=True)

    #dist_limit = 100  #100killometers
    exclude_hh = list()
    for hidx in list(dist_df.index)[1:]:
        #print(hidx)
        prev_lat = float(dist_df.loc[hidx-1].latitude_nm)
        prev_lon = float(dist_df.loc[hidx-1].longitude_nm)
        curr_lat = float(dist_df.loc[hidx].latitude_nm)
        curr_lon = float(dist_df.loc[hidx].longitude_nm)

        dist_diff = abs(geodesic((prev_lat,prev_lon),(curr_lat,curr_lon)).kilometers)
        if dist_diff >= dist_limit:
            exclude_hh.append(hidx)

    if len(exclude_hh) > 0:
        dist_df = dist_df.drop(exclude_hh)
        dist_df = dist_df.reset_index(drop=True)

    return dist_df



def adjust_dist(dist_df, dist_limit):
    '''remove observations, the distance difference between two consecutive locations is greater than 100 km.'''
    dist_df = dist_df.sort_values("hh")
    dist_df = dist_df.reset_index(drop=True)

    normal_idx=[0]
    abnormal_idx=[]
    for hidx in list(dist_df.index)[1:]:
        # print(normal_idx[-1])
        prev_lat = float(dist_df.loc[normal_idx[-1]].latitude_nm)
        prev_lon = float(dist_df.loc[normal_idx[-1]].longitude_nm)
        curr_lat = float(dist_df.loc[hidx].latitude_nm)
        curr_lon = float(dist_df.loc[hidx].longitude_nm)

        dist_diff = abs(geodesic((prev_lat,prev_lon),(curr_lat,curr_lon)).kilometers)
        if dist_diff >= dist_limit*(hidx-normal_idx[-1]):
            abnormal_idx.append(hidx)
        else:
            normal_idx.append(hidx)

    if len(abnormal_idx) > 0:
        dist_df = dist_df.drop(abnormal_idx)
        dist_df = dist_df.reset_index(drop=True)
    return dist_df
