#!/root/anaconda3/bin/python
'''hourly batch
    분석 결과 파일 생성 되지 않았을 경우를 체크하여 시간별 batch
'''

import sys,os
import pandas as pd
import numpy as np
import random
import collections
from tqdm import tqdm
import pickle
import itertools
import mysql.connector as mariadb
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
from hpkg.hservice_util2 import *

#check current date and time
now=datetime.now().strftime("%H:%M:%S")
siteid="01"
print("="*20)
print("siteid: "+siteid+" Hourly JOB START : "+now[0:2])

ymd=date.today().strftime("%Y%m%d")
yy=int(utilfuncs.genFNM(siteid,ymd,1)[10:14])
mm=int(utilfuncs.genFNM(siteid,ymd,1)[14:16])
ddt=int(utilfuncs.genFNM(siteid,ymd,1)[16:18])
print("="*20)
print(ymd)
print(yy,mm,ddt)


# 0. result file check
rchk=utilfuncs.checkfile(siteid,ymd,7)
if rchk==1:
    print("Result File was alread generated!!")
else:
    os.system("/root/hservice/skt_01_20200619.py")
