import sys,os
import pandas as pd
import numpy as np
import random
import collections
from tqdm import tqdm
import pickle
import itertools
import mysql.connector as mariadb
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
from hpkg.hservice_util2 import *

siteid="01"
print("="*20)
print("JOB START")

# 0. 데이터분석 일자
#ymd=date.today().strftime("%Y%m%d")
# ymd="20191116"
# ymd="20191117"
# ymd="20191118"
# ymd="20191119"
# ymd="20191120"
# ymd="20191121"
# ymd="20191122"
# ymd="20191123"
# ymd="20191124"
# ymd="20191125"
# ymd="20191126"
ymd="20191127"
# ymd="20191128"
# ymd="20191129"
# ymd="20191130"

#ymd=date.today().strftime("%Y%m%d")
yy=int(ymd[0:4])
mm=int(ymd[4:6])
ddt=int(ymd[6:8])
print(ymd)
print(utilfuncs.genFNM(siteid,ymd,1))
print(utilfuncs.genFNM(siteid,ymd,2))
print(utilfuncs.genFNM(siteid,ymd,3))
print(utilfuncs.genFNM(siteid,ymd,4))
print(utilfuncs.genFNM(siteid,ymd,5))
print(utilfuncs.genFNM(siteid,ymd,6))

# 0. input file check
filecheck1=os.listdir("/shared/data/common/")
filecheck2=os.listdir("/shared/data/skt/")
f1=np.where(utilfuncs.genFNM(siteid,ymd,1) in filecheck1,1,0)
f2=np.where(utilfuncs.genFNM(siteid,ymd,2) in filecheck2,1,0)
f3=np.where(utilfuncs.genFNM(siteid,ymd,3) in filecheck2,1,0)
f4=np.where(utilfuncs.genFNM(siteid,ymd,4) in filecheck2,1,0)
f5=np.where(utilfuncs.genFNM(siteid,ymd,5) in filecheck2,1,0)
f6=np.where(utilfuncs.genFNM(siteid,ymd,6) in filecheck2,1,0)
checkNum=f1+f2+f3+f4+f5+f6
print(checkNum)

#dt_info11=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,2),sep=",",header=None)
dt_info12=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,3),sep=",",header=None)
#dt_info21=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,4),sep=",",header=None)
#dt_info22=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,5),sep=",",header=None)
#dt_info11.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
#dt_info21.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
dt_info12.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]
#dt_info22.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]

collections.Counter(dt_info12.mbl_phon_num)


# # 적재 확인
mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
tsql="select * from communication"
commu=pd.read_sql(tsql,mariadb_connection)
tsql1="select * from home"
home=pd.read_sql(tsql1,mariadb_connection)

