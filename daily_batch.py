#!/root/anaconda3/bin/python
'''service_site_info 테이블 읽어서 fla=Y인 지역에 대해서 코드 수행
'''
import sys,os
import pandas as pd
import numpy as np
import random
import collections
from tqdm import tqdm
import pickle
import itertools
import mysql.connector as mariadb
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
from hpkg.hservice_util2 import *

#
# READ service_site_info
con = mariadb.connect(user='hanbit', password='game12#',host="10.36.37.102",port="3306",database="silver_care")
tsql = "select * from service_site_info"
stbl = pd.read_sql(tsql,con)
stbl = stbl.reset_index(drop="True")

site_lst = list(stbl[stbl.flag=="Y"].site)

for sidx in site_lst:
    #check current date and time
    # now=datetime.now().strftime("%H:%M:%S")
    
    # print("="*20)
    # print("siteid: "+sidx+" Daily JOB START")

    ymd=date.today().strftime("%Y%m%d")
    # yy=int(utilfuncs.genFNM(sidx,ymd,1)[10:14])
    # mm=int(utilfuncs.genFNM(sidx,ymd,1)[14:16])
    # ddt=int(utilfuncs.genFNM(sidx,ymd,1)[16:18])
    # print("="*20)
    # print(ymd)
    # print(yy,mm,ddt)

    ## 00. delete log files by every the first day of a month
    if ymd[6:]=="01":
        os.system("rm -rf /root/cron.log")

    os.system("/root/hservice/code_template.py "+ sidx)
