#!/root/anaconda3/bin/python
'''서비스 대상자별 시간대별 재실여부 판단 입력
    1. 서비스 대상자 자택 집주소의 위경도 (/shared/data/common/common_siteid_yyyymmdd.csv)
    2. 서비스 대상자의 시간대별 통화건수, SMS, 데이터 사용량, 위도, 경도 (셀기반) 
        (/shared/data/skt/skt_siteid_yyyymmdd_in_{1,2}.csv)
    3. 서비스 대상자의 시간대별 GPS (6분간격저장 (/shared/data/skt/other_siteid_yyyymmdd_in.csv))

결과 파일 저장 : /shared/data/skt/com_siteid_yyyymmdd_out.csv (휴대폰번호,일자,시간,통화건수,sms발신건수,데이터사용량,위도,경도,재실여부)
재실여부 : 0 재실, 1 외출
'''

import sys,os
import pandas as pd
import numpy as np
import random
import collections
from tqdm import tqdm
import pickle
import itertools
import mysql.connector as mariadb
from datetime import date,timedelta,datetime
from geopy.distance import geodesic
from hpkg.hservice_util2 import *

siteid="04"
print("="*20)
print("JOB START")

# 0. 데이터분석 일자
#ymd=date.today().strftime("%Y%m%d")
# ymd="20191116"
# ymd="20191117"
# ymd="20191118"
# ymd="20191119"
# ymd="20191120"
# ymd="20191121"
# ymd="20191122"
# ymd="20191123"
# ymd="20191124"
# ymd="20191126"
# ymd="20191127"
# ymd="20191128"
ymd=date.today().strftime("%Y%m%d")
print(ymd)

# 0. input file check
filecheck1=os.listdir("/shared/data/common/")
filecheck2=os.listdir("/shared/data/skt/")
f1=np.where(utilfuncs.genFNM(siteid,ymd,1) in filecheck1,1,0)
f2=np.where(utilfuncs.genFNM(siteid,ymd,2) in filecheck2,1,0)
f3=np.where(utilfuncs.genFNM(siteid,ymd,3) in filecheck2,1,0)
f4=np.where(utilfuncs.genFNM(siteid,ymd,4) in filecheck2,1,0)
f5=np.where(utilfuncs.genFNM(siteid,ymd,5) in filecheck2,1,0)
f6=np.where(utilfuncs.genFNM(siteid,ymd,6) in filecheck2,1,0)
checkNum=f1+f2+f3+f4+f5+f6
print(checkNum)

if checkNum==6:
    # 1. 서비스 대상자
    add_info=pd.read_csv("/shared/data/common/"+utilfuncs.genFNM(siteid,ymd,1),sep=",")

    # 2. Read DT input files
    dt_info11=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,2),sep=",",header=None)
    dt_info12=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,3),sep=",",header=None)
    dt_info21=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,4),sep=",",header=None)
    dt_info22=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,5),sep=",",header=None)
    dt_info11.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info21.columns=["mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","hh","dt"]
    dt_info12.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]
    dt_info22.columns=["mbl_phon_num","sms_send_cnt","call_send_cnt","total_traffic","hh","dt"]

    skt_input=pd.concat([dt_info11,dt_info21],axis=0)
    skt_feature=pd.concat([dt_info12,dt_info22],axis=0)

    # reformat hh
    hhlst=list(map(str,skt_feature.hh))
    hhlst2=[]
    for hdx in hhlst:
        if len(hdx)==1:
            hhlst2.append('0'+hdx)
        else:
            hhlst2.append(hdx)
    skt_feature.hh=pd.Series(hhlst2)
    skt_feature=skt_feature[skt_feature.hh!="\\N"]

    print(skt_input.dt.unique())
    print(skt_feature.dt.unique())

    threshold=200
    yy=int(ymd[0:4])
    mm=int(ymd[4:6])
    ddt=int(ymd[6:8])
    skt_total=utilfuncs.Load_Data_SKT(skt_input,skt_feature,threshold,yy,mm,ddt)

    #reformat phone number
    skt_total["phone_num"]=pd.Series(list(map(str,skt_total.mbl_phon_num)))
    skt_total["mbl_phon_num"]=skt_total.apply(lambda row:utilfuncs.makeNum(row["phone_num"]),axis=1)
    skt_total=skt_total.drop(columns=["phone_num"])

    #reformat hh
    skt_total["hh"]=pd.Series(list(map(int,skt_total.hh)))

    # 3. Read GPS Info
    gps_info=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,6),sep=",")
    gps_info["hh"]=pd.to_datetime(gps_info["date"]).dt.hour
    gps_info["date2"]=pd.to_datetime(gps_info["date"]).dt.date

    # calculate the representative GPS for each hour
    pps=gps_info.phone.unique()
    gpsDF=pd.DataFrame()
    for pdx in np.arange(len(pps)):
        phoneDF=gps_info[gps_info.phone==pps[pdx]]
        hhs=phoneDF.hh.unique()

        flst=[]
        for hdx in np.arange(len(hhs)):
            dd=phoneDF[phoneDF.hh==hhs[hdx]]
            dd=dd.reset_index(drop=True)
            minnum=min(len(dd),3)
            #mednum=np.where(dd[-minnum:].lat==np.median(dd[-minnum:].lat))[0][0]
            #lval=(dd.phone[mednum],dd.date2[mednum],dd.hh[mednum],dd.lat[mednum],dd.lng[mednum])
            lval=(dd.phone[0],dd.date2[0],dd.hh[0],np.median(dd[-minnum:].lat),np.median(dd[-minnum:].lng))
            flst.append(lval)
        fDF=pd.DataFrame(flst,columns=["phone","dt","hh","hlat","hlon"])
        gpsDF=pd.concat([gpsDF,fDF],axis=0)

    if len(gpsDF)>0:
        # merge fDF, add_info by phone number
        mDF=pd.merge(gpsDF,add_info[["phone","lat","lng"]],on="phone",how="left")
        mDF["home_distance"]=mDF[["hlat","hlon","lat","lng"]].apply(lambda row:utilfuncs.calcDist(row["hlat"],row["hlon"],row["lat"],row["lng"]),axis=1)
        mDF["home_yn"]=np.where(mDF.home_distance<100,0,1)
        mDF=mDF.rename(columns={"phone":"mbl_phon_num","lat":"home_lat","lng":"home_lon","hlat":"latitude_nm","hlon":"longitude_nm"})
        # Reorder columns
        mDF=mDF[["hh","dt","mbl_phon_num","latitude_nm","longitude_nm","home_lat","home_lon","home_distance","home_yn"]]

        # 4. 결과 csv 파일 생성
        dt_info=skt_total[["dt","hh","mbl_phon_num","total_traffic","call_send_cnt","sms_send_cnt"]]
        dt_info=dt_info[dt_info.dt==ymd]
        dt_info["dt"]=(datetime(yy,mm,ddt)-timedelta(1)).strftime("%Y%m%d")
        dt_info["dt"]=pd.to_datetime(dt_info["dt"]).dt.date

        mDF=pd.merge(mDF,dt_info[["dt","hh","mbl_phon_num","total_traffic","call_send_cnt","sms_send_cnt"]],on=["dt","hh","mbl_phon_num"],how="left")
        mDF=mDF[["mbl_phon_num","dt","hh","call_send_cnt","sms_send_cnt","total_traffic","latitude_nm","longitude_nm","home_yn"]]
        mDF.to_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",",index=False )

        # 5. 결과 DB 적재 (communication, home tables)
        mDF=pd.read_csv("/shared/data/skt/"+utilfuncs.genFNM(siteid,ymd,7),sep=",")
        ddt=mDF.dt.unique()[0]
        print(ddt)

        commutbl=mDF[["mbl_phon_num","hh","latitude_nm","longitude_nm","call_send_cnt","sms_send_cnt","total_traffic","dt"]]
        commutbl=commutbl.rename(columns={"mbl_phon_num":"phone","hh":"time","latitude_nm":"lat","longitude_nm":"lng","call_send_cnt":"call","sms_send_cnt":"sms","total_traffic":"data","dt":"measurementDate"})
        commutbl=commutbl.fillna(-999)

        hometbl=mDF[["mbl_phon_num","hh","home_yn","dt"]]
        hometbl=hometbl.rename(columns={"mbl_phon_num":"phone","hh":"time","home_yn":"home","dt":"measurementDate"})

        mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
        # utilfuncs.delData(ddt,"communication",mariadb_connection)
        utilfuncs.loadData(commutbl,"communication",mariadb_connection)
        # utilfuncs.delData(ddt,"home",mariadb_connection)
        utilfuncs.loadData(hometbl,"home",mariadb_connection)
        print("JOB END")

        # # 적재 확인
        # mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
        # tsql="select * from communication"
        # commu=pd.read_sql(tsql,mariadb_connection)
        # tsql1="select * from home"
        # home=pd.read_sql(tsql1,mariadb_connection)

        # mariadb_connection = mariadb.connect(user='hanbit', password='game12#',host="10.41.179.169",port="3306",database="silver_care")
        # pd.read_sql("select count(*) from communication where measurementDate='2019-11-22'",mariadb_connection)
    else:
        print("gps_info has zero row")
else:
    print("check input files")
